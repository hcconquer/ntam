/*===============================================================                          
 * gear.h
 * Created on: 2010-09-14
 * Modify    : 2015-03-23
 * Version   : 11056
 *     Author: chenhan
**===============================================================*/
#ifndef _GEAR_H
#define _GEAR_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <net/if.h>

#ifdef __cplusplus
extern "C" {
#endif

/********************************KERNEL******************************/
/* from linux/compiler.h */
#define __same_type(a, b) __builtin_types_compatible_p(typeof(a), typeof(b))

/* from linux/compiler-gcc.h */
/*
 * kernel style, complex and cool
 * used int[] != &(int *)[0] and int * == &(int *)[0]
 */
#ifdef __cplusplus
#define __must_be_array(a) 0
#else
#define __must_be_array(a) BUILD_BUG_ON_ZERO(__same_type((a), &(a)[0]))
#endif

/* from linux/kernel.h */
#define BUILD_BUG_ON_NOT_POWER_OF_2(n) BUILD_BUG_ON((n) == 0 || (((n) & ((n) - 1)) != 0))

#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))
#define BUILD_BUG_ON_NULL(e) ((void *)sizeof(struct { int:-!!(e); }))

#if !defined BUILD_BUG_ON
#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
#endif

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]) + __must_be_array(arr))

/********************************TYPEDEF******************************/
/*
#ifndef __size_t
typedef unsigned long size_t;
#endif
*/

#if defined(__LP64__)
#if !defined(__APPLE__)
typedef unsigned long uint64_t;
#endif
#endif

#define BYTE_ORDER_BIG_ENDIAN    1
#define BYTE_ORDER_LITTLE_ENDIAN 2
#define BYTE_ORDER_NETWORK       BYTE_ORDER_BIG_ENDIAN
// #if __BYTE_ORDER == __BIG_ENDIAN
// #define BYTE_ORDER_HOST          BYTE_ORDER_BIG_ENDIAN
// #else
#define BYTE_ORDER_HOST          BYTE_ORDER_LITTLE_ENDIAN
// #endif

/********************************RUNNIG******************************/
#if !(defined STACK_SIZE)
#define STACK_SIZE (4 * 1024 * 1024)
#endif


/********************************BASE******************************/
#define ISTYPE(x, y) __same_type((x), (y))
#define SAMETYPE(x, y) __same_type((x), (y))

/*
 * if not array, will be fail when compile
 * if is array, will return 0
 */
#define MUST_BE_ARRAY(arr) __must_be_array(arr)

/*
 * the length of the array, int[3] is 3
 * these is two method to avoid ARRLEN(int *), the param is must be array
 * 1.used anonymous array ((int[]) { })[0]
 *   #define ARRLEN(arr) (sizeof(arr) / sizeof(((typeof(arr)){})[0]))
 
 * 2.kernel style and is complex and cool
 *   used int[] != &(int *)[0] and int * == &(int *)[0]
 *   #define ARRLEN(arr) \
 *	     (sizeof(arr) / sizeof((arr)[0]) \
 *       + sizeof(typeof(int[1 - 2 * !!__builtin_types_compatible_p(typeof(arr), typeof(&arr[0]))]) * 0)
 */
#define ARRLEN(arr) ARRAY_SIZE(arr)

/* add type support, e.g. OFFSETOF(*pst, m) */
#if !defined OFFSETOF
#ifdef __cplusplus
#define OFFSETOF(st, m) (((size_t)(&((typeof(st) *)1)->m)) - 1)
#else
#define OFFSETOF(st, m) (size_t)(&((typeof(st) *)0)->m)
#endif
#endif

/* the size of the structure's element */
#define SZSTMEM(st, m) sizeof(((typeof(st) *) 0)->m)

/* the size between pointers */
#define OFFSET(p, b) ((ssize_t)((size_t) (p) - (size_t) (b)))

/* if the p is arr, use like OFFOF((void *) arr, n) */
#ifdef __cplusplus
#define OFFOF(p, n) ((typeof(p)) ((char *) (p) + (n)))
#else
#define OFFOF(p, n) ((typeof(p)) ((void *) (p) + (n)))
#endif

/* to expand macro value first */
#define _MACRO_STR(v)   (#v)
#define MACRO_STR(v)    _MACRO_STR(v)

#if !defined(STR_EMPTY)
#define STR_EMPTY(str)        ((str == NULL) || (str[0] == '\0'))
#endif

#if !defined(STR_CLEAR)
#define STR_CLEAR(str)        ((str != NULL) && (str[0] = '\0'))
#endif

#define WEAK_VAR(name, type)           __attribute__((weak)) type name
#define WEAK_VAR_INIT(name, type, val) __attribute__((weak)) type name = val

#if defined(__APPLE__)
#define WEAK_FUNC_0(name, prx, rt)                 rt __attribute__((weak)) name()
#define WEAK_FUNC_1(name, prx, rt, p1)             rt __attribute__((weak)) name(p1)
#define WEAK_FUNC_2(name, prx, rt, p1, p2)         rt __attribute__((weak)) name(p1, p2)
#define WEAK_FUNC_3(name, prx, rt, p1, p2, p3)     rt __attribute__((weak)) name(p1, p2, p3)
#define WEAK_FUNC_4(name, prx, rt, p1, p2, p3, p4) rt __attribute__((weak)) name(p1, p2, p3, p4)
#else
#define WEAK_FUNC_0(name, prx, rt)                 rt prx##name()
#define WEAK_FUNC_1(name, prx, rt, p1)             rt prx##name(p1)
#define WEAK_FUNC_2(name, prx, rt, p1, p2)         rt prx##name(p1, p2)
#define WEAK_FUNC_3(name, prx, rt, p1, p2, p3)     rt prx##name(p1, p2, p3)
#define WEAK_FUNC_4(name, prx, rt, p1, p2, p3, p4) rt prx##name(p1, p2, p3, p4)
#endif

#if defined(__linux__)
#define ALIAS_FUNC_0(an, name, rt)                 rt an()               __attribute__((weak, alias(#name)))
#define ALIAS_FUNC_1(an, name, rt, p1)             rt an(p1)             __attribute__((weak, alias(#name)))
#define ALIAS_FUNC_2(an, name, rt, p1, p2)         rt an(p1, p2)         __attribute__((weak, alias(#name)))
#define ALIAS_FUNC_3(an, name, rt, p1, p2, p3)     rt an(p1, p2, p3)     __attribute__((weak, alias(#name)))
#define ALIAS_FUNC_4(an, name, rt, p1, p2, p3, p4) rt an(p1, p2, p3, p4) __attribute__((weak, alias(#name)))
#elif defined(__CYGWIN__)
#define ALIAS_FUNC_0(an, name, rt)                 rt an()               __attribute__((alias(#name)))
#define ALIAS_FUNC_1(an, name, rt, p1)             rt an(p1)             __attribute__((alias(#name)))
#define ALIAS_FUNC_2(an, name, rt, p1, p2)         rt an(p1, p2)         __attribute__((alias(#name)))
#define ALIAS_FUNC_3(an, name, rt, p1, p2, p3)     rt an(p1, p2, p3)     __attribute__((alias(#name)))
#define ALIAS_FUNC_4(an, name, rt, p1, p2, p3, p4) rt an(p1, p2, p3, p4) __attribute__((alias(#name)))
#elif defined(__APPLE__)
#define ALIAS_FUNC_0(an, name, rt) 
#define ALIAS_FUNC_1(an, name, rt, p1)
#define ALIAS_FUNC_2(an, name, rt, p1, p2) 
#define ALIAS_FUNC_3(an, name, rt, p1, p2, p3) 
#define ALIAS_FUNC_4(an, name, rt, p1, p2, p3, p4) 
#else
#define ALIAS_FUNC_0(an, name, rt) 
#define ALIAS_FUNC_1(an, name, rt, p1) 
#define ALIAS_FUNC_2(an, name, rt, p1, p2) 
#define ALIAS_FUNC_3(an, name, rt, p1, p2, p3) 
#define ALIAS_FUNC_4(an, name, rt, p1, p2, p3, p4) 
#endif

#ifndef SWAP
#define SWAP(m, n) \
({ \
 	typeof(m) _t; \
 	_t = (m); \
 	(m) = (n); \
 	(n) = _t;\
})
#endif

#define ABS(v) \
({ \
	typeof(v) _v = (v); \
	if (_v < 0) { \
		_v = -_v; \
	} \
	_v; \
})

#ifndef BITSET
#define BITSET(b, i, v) (((b) & ~(1UL << (i))) | ((!!(v)) << (i)))
#endif

#ifndef BITGET
#define BITGET(b, i)    (!!((b) & (1UL << (i))))
#endif

/* map, size, index, offset, value */
#define BITMAP_SET(m, s, i, o, v) \
({ \
    uint8_t *_m = (uint8_t *) m; \
    typeof(s) _s = s; \
    typeof(i) _i = i; \
    typeof(o) _o = o; \
    typeof(v) _v = v; \
    off_t _t = _s * _i + _o; \
    _m = OFFOF(_m, _t / 8); \
	*_m = BITSET(*_m, _t % 8, !!(_v)); \
	_v; \
})

/* map, size, index, offset, value */
#define BITMAP_GET(m, s, i, o) \
({ \
    uint8_t *_m = (uint8_t *) m; \
    typeof(s) _s = s; \
    typeof(i) _i = i; \
    typeof(o) _o = o; \
    int _v = 0; \
    off_t _t = _s * _i + _o; \
    _m = OFFOF(_m, _t / 8); \
	_v = BITGET(*_m, _t % 8); \
    _v; \
})

#define _DO_DEBUG_MORE(fp, fmt, args...) do { struct timeval _tv; struct tm _tm; char _ts[32]; gettimeofday(&_tv, NULL); _tm = *localtime(&_tv.tv_sec); strftime(_ts, sizeof(_ts), "%Y-%m-%d %H:%M:%S", &_tm); fprintf(fp, "[%s.%06ld] %s:%d(%s): "fmt"\n", _ts, (long) _tv.tv_usec, __FILE__, __LINE__, __func__, ##args); } while (0)
#define _DO_DEBUG_LESS(fp, fmt, args...) do { fprintf(fp, ""fmt"\n", ##args); } while (0)
#define _DO_DEBUG_NORM(fp, fmt, args...) do { struct timeval _tv; struct tm _tm; char _ts[32]; gettimeofday(&_tv, NULL); _tm = *localtime(&_tv.tv_sec); strftime(_ts, sizeof(_ts), "%Y-%m-%d %H:%M:%S", &_tm); fprintf(fp, "[%s.%06ld]: "fmt"\n", _ts, (long) _tv.tv_usec, ##args); } while (0)

#if defined(_DEBUG_MORE)
#define _DO_DEBUG(fp, fmt, args...) do { _DO_DEBUG_MORE(fp, fmt, ##args); } while (0)
#elif defined(_DEBUG_LESS)
#define _DO_DEBUG(fp, fmt, args...) do { _DO_DEBUG_LESS(fp, fmt, ##args); } while (0)
#elif defined(_DEBUG)
#define _DO_DEBUG(fp, fmt, args...) do { _DO_DEBUG_NORM(fp, fmt, ##args); } while (0)
#else
#define _DO_DEBUG(fp, fmt, args...) do { } while (0)
#endif // _DEBUG_MORE

#if !defined(DEBUGS)
#ifdef _DEBUG_FILE // _DEBUG_FILE
#define DEBUGS(fmt, args...) do { FILE *_f = fopen(MACRO_STR(_DEBUG_FILE), "a"); if (_f != NULL) { _DO_DEBUG(_f, fmt, ##args); fclose(_f); } } while (0)
#define DEBUGXXD(buf, len) do { char str[256]; int i = 0; FILE *_f = fopen(MACRO_STR(_DEBUG_FILE), "a"); if (_f != NULL) { for (i = 0; i < len; i += 16) { snprintfxxd(str, sizeof(str), OFFOF(buf, i), 16); _DO_DEBUG_LESS(stdout, "%s", str); } fclose(_f); } } while (0)
#else // _DEBUG_FILE
#define DEBUGS(fmt, args...) do { _DO_DEBUG(stdout, fmt, ##args); } while (0)
#define DEBUGXXD(buf, len) do { char str[256]; int i = 0; for (i = 0; i < len; i += 16) { snprintfxxd(str, sizeof(str), OFFOF(buf, i), 16); _DO_DEBUG_LESS(stdout, "%s", str); } } while (0)
#endif // _DEBUG_FILE
#endif // DEBUG

typedef struct {
	int key;
	int value;
} int_pair_t;

typedef struct {
	int  key;
	void *value;
} int_ptr_pair_t;

typedef struct {
	uint32_t key;
	uint32_t value;
} uint32_pair_t;

typedef struct {
    void *key;
    void *value;
} ptr_pair_t;

#define MAP_GET_EX(_map, _key, _idx) \
({ \
    int idx = _idx, i = 0; \
    typeof(_key) key = _key; \
    for (i = 0; i < (int) ARRLEN(_map); i++) { \
        if (key == _map[i].key) { \
            idx = i; \
        } \
    } \
    idx; \
})

#define MAP_GET(_map, _key) MAP_GET_EX(_map, _key, -1)

/********************************UTIL*******************************/
char *strncpy_v(char *dst, const char *src, size_t num);
/* get the file size */
off_t filesize(char *filepath);
off_t fsize(int fd);
void msleep(int msec);


/*******************************PROC*******************************/
int daemonize(const char *dir, int noclose);
int set_core_dump();
int set_core_size(int size);
int set_stack_size(int size);
/*
 * get all pid expect current process
 * ppid can be NULL
**/
int get_pids_by_name(pid_t *pids, int max, const char *name);
/*
 * kill process with special signo
 * just send signal to process only once
 *
 * wait != 0, like bool true, wait until all process exit or not exist
 *
 * if signo is valid, will send signal to all process at first
 * even get some process has no auth
 *
 * if any one process no auth to kill but still wait all
 * process which has auth exist
 *
 * in one words, kill process can kill
 *
 * return:
 * 0, is kill all process
 * 1, kill but current process had been exist by g_terminated
 * -1, failed because signo is invalid or no auth to kill some process
**/
int kill_procs(pid_t *pids, int num, int signo, int wait);

WEAK_VAR_INIT(g_terminated, volatile int, 0);
void terminate(int signo);
int sig_ignore(int sig);
void RegSignal();
/*
 * e.g.
 *  sig_reg(terminate, SIGINT, SIGQUIT, SIGTERM, 0);
    sig_reg(SIG_IGN, SIGHUP, 0);
 *
 * terminate:
 * SIGINT:  <CTRL_C>
 * SIGQUIT: <CTRL_\>
 * SIGTERM: <kill>
 *
 * SIG_IGN:
 * SIGHUP
 */
int sig_reg_s(void (*handler)(int), int *sigs, int num);
int sig_reg(void (*handler)(int), ...);


/********************************SEQUENCE*******************************/
int snprintfxxd(char *str, size_t strlen, const void *buf, int len);


/********************************IO*******************************/
#ifndef BUF_UDP_MAX
#define BUF_UDP_MAX (65535)
#endif

#ifndef BUF_TCP_MAX
#define BUF_TCP_MAX (65535)
#endif

#ifndef BUF_NET_MAX
#define BUF_NET_MAX (65535)
#endif

#define FD_EV_RD  (1 << 0) // 1
#define FD_EV_WR  (1 << 1) // 2
#define FD_EV_ERR (1 << 2) // 4
#define FD_EV_ALL (FD_EV_RD | FD_EV_WR | FD_EV_ERR)

#define FD_FLAG_EOF     1 // read eof
#define FD_FLAG_CLOSED  2 // write bad file

/* 
 * simple for select system call
 * ignore some signals except define in the params which end with 0
 *
 * about write:
 * if the socket has been closed by the other side, now you can to wirte
 * and without error event until you close write self
 *
 * On success, return the ev get
 * else, return -1 
**/
int WaitForFd(int fd, int ev, long msec, ...);

/* 
 * read until count == len,
 * ignore some signals except define in the params which end with 0
 * timeo is max time when read, if < 0, will read until enough
 *
 * need to set fd to nonblock by yourself before called
 *
 * if read len or get some unknown errno, return the count readed
 * if read EOF, return -count
**/
ssize_t ReadEx(int fd, void *buf, size_t len, long timeo, int *flags, ...);

/* 
 * write until count == len,
 * ignore some signals except define in the params which end with 0
 * timeo is max time when write, if < 0, will write until read enough
 *
 * need to set fd to nonblock by yourself before called
 *
 * if no byte wrote, will return -1
*/ 
ssize_t WriteEx(int fd, const void *buf, size_t len, long timeo, int *flags, ...);


/*****************************SOCKET****************************/
typedef struct {
    char      name[IFNAMSIZ];
    in_addr_t addr;
    in_addr_t broadaddr;
    in_addr_t netmask;
} eth_addr_t;

/*
 * 1.check ip, is localhost, broadcast or other resverse ip
 * 2.check broadcast, is 0.0.0.0 or 255.255.255.255 
 *
 * return 
 * 0, false
 * other, true
**/
int IsResvEth(const eth_addr_t *eth);
int IsLanEth(const eth_addr_t *eth);
int GetLanEths(eth_addr_t *eths, int num, const char *name);
int GetWanEths(eth_addr_t *eths, int num, const char *name);
/*
 * name, eth name, if NULL, will get all eths
 *
 * return
 * <  0, fail
 * >= 0, eth num
**/
int GetEths(eth_addr_t *eths, int num, const char *name);

/*
 * locaddr, local bind ip, host order, INADDR_ANY not bind
 * locport, local bind port, host order, 0 not bind
 * local host or port can be bind only one
 *
 * svraddr, remote connect ip, INADDR_ANY not connect
 * svrport, remote connect port, 0 not connect
 * remote host and port must be exist at same time and can be connect
 *
 * type, now support SOCK_DGRAM(udp) or SOCK_STREAM(tcp)
 *
 * timeo, connect timeout
 * if < 0, will block until connect system call terminate
 * if >= 0, will block timeo at most
 *
 * return
 * >=  0, success
 * == -1, bad params
 * == -2, create socket fail
 * == -3, local bind fail
 * == -4, remote connect fail
 * == -5, remote connect wait timeout, not connected until timeout
 * == -6, set socket opt fail
**/
int CreateSock(in_addr_t locaddr, int locport, in_addr_t svraddr, int svrport, int type, int timeo);
int CheckSockConnect(int sock, long msec);
int SetSockTimeout(int sock, int type, int msec);
int SetSockBlock(int sock, int block);
int SetNonBlock(int sock);
int GetSockType(int sock);


/****************************TIME******************************/
#define TIME_YEAR	(1 << 1)
#define TIME_MON 	(1 << 2)
#define TIME_DAY	(1 << 3)
#define TIME_SEC	(1 << 4)
#define TIME_MS		(1 << 5)
#define TIME_US 	(1 << 6)

#define TIME_FMT_DATETIME (1)
#define TIME_FMT_DATE     (2)
#define TIME_FMT_TIME     (3)

WEAK_VAR(g_time, struct timeval);
WEAK_VAR(g_zone, struct timezone);

long TimeGet(int flag);
long TimeDiff(struct timeval *tv, struct timeval *base);

#ifdef __cplusplus
}
#endif

#endif /* GEAR_H_ */

