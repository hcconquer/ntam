/*
 * server.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: hanchen
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <getopt.h>
#include <libgen.h>
#if ! defined(__APPLE__)
// #else
#include <endian.h>
#endif

#include "gear.h"

#include "CNP_Protocol1.h"

using namespace cnp;

#define RUN_MODE_START   1
#define RUN_MODE_STOP    2
#define RUN_MODE_RESTART 3

#define VERBOSE_NONE      (0)
#define VERBOSE_OPTS      (1 << 0)
#define VERBOSE_TIME      (1 << 1)
#define VERBOSE_SEND      (1 << 2)
#define VERBOSE_RECV      (1 << 3)
#define VERBOSE_ALL       (0xFFFF)

#define OPTV_HELP           'h'
#define OPTV_VERSION        'v'
#define OPTV_VERBOSE        's'
#define OPTV_CMD            'c'
#define OPTV_TIMEO          't'
#define OPTV_HOST           130
#define OPTV_PORT           131
#define OPTV_CLIENT_ID      140
#define OPTV_FIRST_NAME     141
#define OPTV_LAST_NAME      142
#define OPTV_DL_NUMBER      143
#define OPTV_SS_NUMBER      144
#define OPTV_PIN            145
#define OPTV_AMOUNT         146
#define OPTV_START          147
#define OPTV_COUNT          148
#define OPTV_DEPOSIT_TYPE   149

typedef struct {
    int (*before)(void *args);
    int (*handle)(void *args);
    int (*after)(void *args);
} proc_func_t;

typedef struct {
    int         key;
    proc_func_t value;
} int_proc_pair_t;

typedef struct {
	ssize_t (*pack_rst)(void *args, void *, size_t);
	ssize_t (*parse_rsp)(void *args, void *, size_t);
} handle_func_t;

typedef struct {
    int           key;
    handle_func_t value;
} int_handle_pair_t;

typedef struct
{
    /* config */
    int  daemon;
    int  verbose;
    int  cmd;
    int  timeo;

    in_addr_t svr_addr;
    int svr_port;

    /* context */
    int run_mode;
    char proc_name[128];

    int svr_udp_sock;

    int client_id;
    char first_name[MAX_NAME_LEN];
    char last_name[MAX_NAME_LEN];
    long dl_number;
    long ss_numver;
    char pin[MAX_PIN_LEN];
    long amount;
    // int count;
    int start;
    int count;
    int deposit_type;

    char zeros[1]; // for some struct not need to memset
} Config;

static Config config[1];

static int ConnectToServer(void *arg) {
	Config *config = (typeof(config)) arg;

	int sock = -1, ret = -1;
    sock = CreateSock(INADDR_ANY, -1, config->svr_addr, config->svr_port, SOCK_DGRAM, config->timeo);
	if (sock < 0) {
        DEBUGS("connect fail, ret: %d", sock);
		return -2;
	}
    ret = SetSockBlock(sock, 0);
    if (ret < 0) {
        DEBUGS("set sock nonblock fail, ret: %d", ret);
        close(sock);
        return -3;
    }
    config->svr_udp_sock = sock;
    return 0;
}

static int CloseServerConn(void *arg) {
	Config *config = (typeof(config)) arg;

    if (config->svr_udp_sock >= 0) {
        close(config->svr_udp_sock);
        config->svr_udp_sock = -1;
    }
    return 0;
}

static ssize_t SendRecvUdp(int fd, void *rbuf, size_t rlen, const void *sbuf, size_t slen) {
    ssize_t len = WriteEx(fd, sbuf, slen, 0, NULL, EINTR, EAGAIN, 0);
    if (config->verbose & VERBOSE_SEND) {
        DEBUGS("send, ret: %zd, len: %zd", len, slen);
    }
    if (len < 0) {
        DEBUGS("send fail, ret: %zd, errno: %d, socket: %d", len, errno, fd);
        return -2;
    }

    len = ReadEx(fd, rbuf, rlen, config->timeo, NULL, EINTR, EAGAIN, 0);
    if (config->verbose & VERBOSE_RECV) {
        DEBUGS("recv, len: %zd", len);
    }
    if (len < (int) sizeof(CNP_STD_HDR)) {
        DEBUGS("recv fail, len: %zd", len);
        return -3;
    }

    return len;
}

static ssize_t PackConnectRst(void *arg, void *sbuf, size_t slen) {
	CNP_CONNECT_REQUEST req;
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseConnectRsp(void *args, void *rbuf, size_t rlen) {
	CNP_CONNECT_RESPONSE *resp = (typeof(resp)) rbuf;
	CONNECTION_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);
	DEBUGS("major_version: %d", body->m_wMajorVersion);
	DEBUGS("minor_version: %d", body->m_wMinorVersion);
	DEBUGS("client_id: %d", body->m_wClientID);

	return 0;
}

static ssize_t PackCreateAccountRst(void *arg, void *sbuf, size_t slen) {
	CNP_CREATE_ACCOUNT_REQUEST req(config->client_id);
	CNP_STD_HDR *head = &req.m_Hdr;
	CREATE_ACCOUNT_REQUEST *body = &req.m_Request;
	ssize_t len = 0;

	strncpy_v(body->m_szFirstName, config->first_name, sizeof(body->m_szFirstName));
	strncpy_v(body->m_szLastName, config->last_name, sizeof(body->m_szLastName));
	body->m_dwDLNumber = config->dl_number;
	body->m_dwSSNumber = config->ss_numver;
	strncpy_v(body->m_szPIN, config->pin, sizeof(body->m_szPIN));

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseCreateAccountRsp(void *args, void *rbuf, size_t rlen) {
	CNP_CREATE_ACCOUNT_RESPONSE *resp = (typeof(resp)) rbuf;
	CREATE_ACCOUNT_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	if (body->m_dwResult == CER_SUCCESS) {
		DEBUGS("client_id: %d", body->m_wClientID);
	}

	return 0;
}

static ssize_t PackLogonRst(void *arg, void *sbuf, size_t slen) {
	CNP_LOGON_REQUEST req(config->client_id);
	CNP_STD_HDR *head = &req.m_Hdr;
	LOGON_REQUEST *body = &req.m_Request;
	ssize_t len = 0;

	strncpy_v(body->m_szFirstName, config->first_name, sizeof(body->m_szFirstName));
	strncpy_v(body->m_szPIN, config->pin, sizeof(body->m_szPIN));

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseLogonRsp(void *args, void *rbuf, size_t rlen) {
	CNP_LOGON_RESPONSE *resp = (typeof(resp)) rbuf;
	LOGON_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	return 0;
}

static ssize_t PackLogoffRst(void *arg, void *sbuf, size_t slen) {
	CNP_LOGOFF_REQUEST req(config->client_id);
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseLogoffRsp(void *args, void *rbuf, size_t rlen) {
	CNP_LOGOFF_RESPONSE *resp = (typeof(resp)) rbuf;
	LOGOFF_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	return 0;
}

static ssize_t PackDepositRst(void *arg, void *sbuf, size_t slen) {
	CNP_DEPOSIT_REQUEST req(config->client_id, config->amount, (DEPOSIT_TYPE) config->deposit_type);
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	// DEBUGS("type: %d", req.m_Request.m_wType);

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseDepositRsp(void *args, void *rbuf, size_t rlen) {
	CNP_DEPOSIT_RESPONSE *resp = (typeof(resp)) rbuf;
	DEPOSIT_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	return 0;
}

static ssize_t PackWithdrawalRst(void *arg, void *sbuf, size_t slen) {
	CNP_WITHDRAWAL_REQUEST req(config->client_id, config->amount);
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseWithdrawalRsp(void *args, void *rbuf, size_t rlen) {
	CNP_WITHDRAWAL_RESPONSE *resp = (typeof(resp)) rbuf;
	WITHDRAWAL_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	return 0;
}

static ssize_t PackBalanceQueryRst(void *arg, void *sbuf, size_t slen) {
	CNP_BALANCE_QUERY_REQUEST req(config->client_id);
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseBalanceQueryRsp(void *args, void *rbuf, size_t rlen) {
	CNP_BALANCE_QUERY_RESPONSE *resp = (typeof(resp)) rbuf;
	BALANCE_QUERY_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	if (body->m_dwResult == CER_SUCCESS) {
		DEBUGS("balance: %d", body->m_dwBalance);
	}

	return 0;
}

static ssize_t PackTransactionQueryRst(void *arg, void *sbuf, size_t slen) {
	CNP_TRANSACTION_QUERY_REQUEST req(config->client_id);
	CNP_STD_HDR *head = &req.m_Hdr;
	TRANSACTION_QUERY_REQUEST *body = &req.m_Request;
	ssize_t len = 0;

	body->m_dwStartID = config->start;
	body->m_wTransactionCount = config->count;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseTransactionQueryRsp(void *args, void *rbuf, size_t rlen) {
	CNP_TRANSACTION_QUERY_RESPONSE *resp = (typeof(resp)) rbuf;
	TRANSACTION_QUERY_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	if (body->m_dwResult == CER_SUCCESS) {
		DEBUGS("count: %d", body->m_wTransactionCount);
		for (int i = 0; i < body->m_wTransactionCount; i++) {
			TRANSACTION *trans = &body->m_rgTransactions[i];
			DEBUGS("id: %d, datetime: %lld, amount: %d, type: %d",
					trans->m_dwID, trans->m_qwDateTime, trans->m_dwAmount, trans->m_wType);
		}
	}

	return 0;
}

static ssize_t PackStampPurchaseRst(void *arg, void *sbuf, size_t slen) {
	CNP_STAMP_PURCHASE_REQUEST req(config->client_id, config->count);
	CNP_STD_HDR *head = &req.m_Hdr;
	ssize_t len = 0;

	len = req.get_Size();
	head->m_wDataLen = len;

	memcpy(sbuf, &req, len);

	return len;
}

static ssize_t ParseStampPurchaseRsp(void *args, void *rbuf, size_t rlen) {
	CNP_STAMP_PURCHASE_RESPONSE *resp = (typeof(resp)) rbuf;
	STAMP_PURCHASE_RESPONSE *body = &resp->m_Response;

	DEBUGS("result: %d", body->m_dwResult);

	int idx = MAP_GET_EX(errmap, (int) body->m_dwResult, 0);
	DEBUGS("%s", (char *) errmap[idx].value);

	return 0;
}

static int HandleNetComm(void *arg) {
	Config *config = (typeof(config)) arg;

	int_handle_pair_t handles[] = {
			{ CMT_CONNECT,            { PackConnectRst,            ParseConnectRsp          } },
			{ CMT_CREATE_ACCOUNT,     { PackCreateAccountRst,      ParseCreateAccountRsp    } },
			{ CMT_LOGON,              { PackLogonRst,              ParseLogonRsp            } },
			{ CMT_LOGOFF,             { PackLogoffRst,             ParseLogoffRsp           } },
			{ CMT_DEPOSIT,            { PackDepositRst,            ParseDepositRsp          } },
			{ CMT_WITHDRAWAL,         { PackWithdrawalRst,         ParseWithdrawalRsp       } },
			{ CMT_BALANCE_QUERY,      { PackBalanceQueryRst,       ParseBalanceQueryRsp     } },
			{ CMT_TRANSACTION_QUERY,  { PackTransactionQueryRst,   ParseTransactionQueryRsp } },
			{ CMT_PURCHASE_STAMPS,    { PackStampPurchaseRst,      ParseStampPurchaseRsp    } },
    };

    int hidx = MAP_GET(handles, config->cmd);
	if (hidx < 0) {
	    DEBUGS("cmd not found: %d", config->cmd);
		return -2;
	}

    int_handle_pair_t *handle = &handles[hidx];
    handle_func_t *func = &handle->value;

    char sbuf[65535], rbuf[65536];
	ssize_t slen = 0, rlen = 0;
    memset(sbuf, 0, sizeof(sbuf));

    if (func->pack_rst != NULL) {
    	slen = func->pack_rst(config, sbuf, sizeof(sbuf));
    }
    if (slen > 0) {
    	rlen = SendRecvUdp(config->svr_udp_sock, rbuf, sizeof(rbuf), sbuf, slen);
        if (rlen < 0) {
            DEBUGS("send or recv fail, ret: %zd", rlen);
            return -1;
        }
    } else {
		DEBUGS("pack request fail, ret: %zd", slen);
    }
    // DEBUGS("%zd, %zd", slen, rlen);
    if (func->parse_rsp != NULL) {
        func->parse_rsp(config, rbuf, rlen);
    }

    return 0;
}

static int HandleWork(void *arg)
{
	Config *config = (typeof(config)) arg;
	int ret = 0;

	int_proc_pair_t handles[] = {
            { CMT_CONNECT,           { ConnectToServer, HandleNetComm, CloseServerConn } },
            { CMT_CREATE_ACCOUNT,    { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_LOGON,             { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_LOGOFF,            { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_DEPOSIT,           { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_WITHDRAWAL,        { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_BALANCE_QUERY,     { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_TRANSACTION_QUERY, { ConnectToServer, HandleNetComm, CloseServerConn } },
			{ CMT_PURCHASE_STAMPS,   { ConnectToServer, HandleNetComm, CloseServerConn } },
	};

    int hidx = MAP_GET(handles, config->cmd);
	if (hidx < 0) {
	    DEBUGS("cmd not found: %d", config->cmd);
		return -2;
	}

    int_proc_pair_t *handle = &handles[hidx];

    struct timeval stv, etv;
    if (config->verbose & VERBOSE_TIME) {
    	gettimeofday(&stv, NULL);
    }

    proc_func_t *func = &handle->value;
    if (func->before != NULL) {
        ret = func->before(config);
    }
    if (ret >= 0) {
    	ret = func->handle(config);
    } else {
		DEBUGS("before fail, ret: %d", ret);
    }
    if (func->after != NULL) {
        func->after(config);
    }

	if (config->verbose & VERBOSE_TIME) {
		gettimeofday(&etv, NULL);
	}

	if (config->verbose & VERBOSE_TIME) {
		DEBUGS("time: %ldms", TimeDiff(&etv, &stv));
	}

	return 0;
}

static int InitArgs(void *arg, int argc, char **argv)
{
	Config *config = (typeof(config)) arg;
    static struct option long_options[] = {
		{ "cmd", required_argument, NULL, OPTV_CMD }, // 'c'
        { "timeo", required_argument, NULL, OPTV_TIMEO }, // 't'
        { "svraddr", required_argument, NULL, OPTV_HOST },
        { "svrport", required_argument, NULL, OPTV_PORT },
		{ "cltid", required_argument, NULL, OPTV_CLIENT_ID },
		{ "firstname", required_argument, NULL, OPTV_FIRST_NAME },
		{ "lastname", required_argument, NULL, OPTV_LAST_NAME },
		{ "dlnum", required_argument, NULL, OPTV_DL_NUMBER },
		{ "ssnum", required_argument, NULL, OPTV_SS_NUMBER },
		{ "pin", required_argument, NULL, OPTV_PIN },
		{ "amount", required_argument, NULL, OPTV_AMOUNT },
		{ "start", required_argument, NULL, OPTV_START },
		{ "count", required_argument, NULL, OPTV_COUNT },
		{ "depostype", required_argument, NULL, OPTV_DEPOSIT_TYPE },
        { "help", no_argument, NULL, OPTV_HELP }, // 'h'
    	{ "version", no_argument, NULL, OPTV_VERSION }, // 'v'
    	{ "verbose", optional_argument, NULL, OPTV_VERBOSE }, // 's'
        { NULL, 0, NULL, 0 }
    };
    int ret = 0, optv = 0, option_index = 0;

    strncpy_v(config->proc_name, argv[0], sizeof(config->proc_name));

    if (argc < 2)
    {
        return -1;
    }

    for (;;) {
        optv = getopt_long(argc, argv, "hctv", long_options, &option_index);
        if (optv < 0) {
            break;
        }
        switch (optv) {
        case OPTV_CMD: // 'c'
            if (strcasecmp(optarg, "connect") == 0) {
        		config->cmd = CMT_CONNECT;
            } else if (strcasecmp(optarg, "create_account") == 0) {
        		config->cmd = CMT_CREATE_ACCOUNT;
            } else if (strcasecmp(optarg, "logon") == 0) {
        		config->cmd = CMT_LOGON;
            } else if (strcasecmp(optarg, "logoff") == 0) {
        		config->cmd = CMT_LOGOFF;
            } else if (strcasecmp(optarg, "deposit") == 0) {
        		config->cmd = CMT_DEPOSIT;
            } else if (strcasecmp(optarg, "withdrawal") == 0) {
                config->cmd = CMT_WITHDRAWAL;
            } else if (strcasecmp(optarg, "balance_query") == 0) {
                config->cmd = CMT_BALANCE_QUERY;
            } else if (strcasecmp(optarg, "transaction_query") == 0) {
                config->cmd = CMT_TRANSACTION_QUERY;
            } else if (strcasecmp(optarg, "purchase_stamps") == 0) {
                config->cmd = CMT_PURCHASE_STAMPS;
            }
            break;
        case OPTV_TIMEO: // 't'
        	config->timeo = atoi(optarg);
        	break;
        case OPTV_HOST:
        	config->svr_addr = inet_addr(optarg);
        	break;
        case OPTV_PORT:
        	config->svr_port = atoi(optarg);
        	break;
        case OPTV_CLIENT_ID:
        	config->client_id = atoi(optarg);
        	break;
        case OPTV_FIRST_NAME:
        	strncpy_v(config->first_name, optarg, sizeof(config->first_name));
        	break;
        case OPTV_LAST_NAME:
        	strncpy_v(config->last_name, optarg, sizeof(config->last_name));
        	break;
        case OPTV_DL_NUMBER:
        	config->dl_number = atol(optarg);
        	break;
        case OPTV_SS_NUMBER:
        	config->ss_numver = atol(optarg);
        	break;
        case OPTV_PIN:
        	strncpy_v(config->pin, optarg, sizeof(config->pin));
        	break;
        case OPTV_AMOUNT:
        	config->amount = atol(optarg);
        	break;
        case OPTV_START:
        	config->start = atol(optarg);
        	break;
        case OPTV_COUNT:
        	config->count = atol(optarg);
        	break;
        case OPTV_DEPOSIT_TYPE:
        	if (strcasecmp(optarg, "cash") == 0) {
        		config->deposit_type = DT_CASH;
        	} else if (strcasecmp(optarg, "check") == 0) {
        		config->deposit_type = DT_CHECK;
        	}
        	break;
        case OPTV_HELP: // help
        	ret = 1;
        	break;
        case OPTV_VERSION: // version
        	ret = 2;
        	break;
        case OPTV_VERBOSE: // 's'
        	if (optarg != NULL) {
        		int v = atoi(optarg);
        		if (v == 0) {
        			config->verbose = 0;
        		} else {
        			config->verbose |= v;
        		}
        	} else {
                config->verbose = VERBOSE_ALL;
        	}
        	break;
        }
        if (ret != 0)
        {
            break;
        }
    }

    return ret;
}

static int InitConfig(void *arg) {
	Config *config = (typeof(config)) arg;

	/*
    eth_addr_t eth;
    memset(&eth, 0, sizeof(eth));
    GetLanEths(&eth, 1, NULL);
    DEBUGS("eth_addr: %s", inet_ntoa(*(struct in_addr * ) &eth.addr));
    */

    if (config->svr_addr == INADDR_ANY) {
    	config->svr_addr = inet_addr("127.0.0.1");
    }

    if (config->svr_port <= 0) {
    	config->svr_port = 5001;
    }

    DEBUGS("svr_addr: %s, svr_port: %d", inet_ntoa(*(struct in_addr * ) &config->svr_addr), config->svr_port);

    if (config->timeo <= 0) {
        config->timeo = 1000;
    }

    return 0;
}

static int InitProc(void *arg) {
	Config *config = (typeof(config)) arg;

    sig_reg(terminate, SIGINT, SIGQUIT, SIGTERM, 0);
    sig_reg(SIG_IGN, SIGHUP, 0);

    set_core_size(100 * 1024 * 1024);

    if (config->daemon > 0) {
        daemonize(NULL, 0);
    }

    return 0;
}

static int Inits(void *arg) {
    const ptr_pair_t ifuncs[] = {
        { (void *) InitConfig,  (void *) "InitConfig" },
		{ (void *) InitProc,    (void *) "InitProc" }
    };

    Config *config = (typeof(config)) arg;
    int ret = 0, i = 0;
    int (*ifunc)(Config *) = NULL;
    char *desc = NULL;
    for (i = 0; i < (int) ARRLEN(ifuncs); i++) {
        ifunc = (typeof(ifunc)) ifuncs[i].key;
        desc = (typeof(desc)) ifuncs[i].value;
        ret = ifunc(config);
        if (ret < 0) {
            printf("%s fail, ret: %d\n", desc, ret);
            return -1;
        }
    }

    return 0;
}

static int PrintUsage(int argc, char **argv) {
    printf("Usage: %s [OPTION]...\n", argv[0]);
    printf("  %3s %-10s %s\n", "-c,", "--cmd",      "command, connect, create_account, logon, logoff, deposit, withdrawal, balance_query, transaction_query, purchase_stamps");
    printf("  %3s %-10s %s\n", "",    "--svraddr",  "server addr");
    printf("  %3s %-10s %s\n", "",    "--svrport",  "server port");
    printf("  %3s %-10s %s\n", "",    "--timeo",    "timeout, default: 1000ms, if < 0, block");
    printf("  %3s %-10s %s\n", "",    "--verbose",  "verbose level, --verbose=0, 0: none; 1: opts; 2: time; 255/none: all;");
    printf("  %3s %-10s %s\n", "-v,",  "--version",  "version");
    printf("  %3s %-10s %s\n", "-h,", "--help",     "help");

    return 0;
}

int main(int argc, char **argv)
{
    int ret = 0;

    memset(config, 0, OFFSETOF(*config, zeros));

    ret = InitArgs(config, argc, argv);
    if (ret < 0) {
        PrintUsage(argc, argv);
        return -1;
    } else if (ret == 1) { // help
        PrintUsage(argc, argv);
        return 0;
    }

    Inits(config);

	// DEBUGS("start");

	HandleWork(config);

	// DEBUGS("exit");

	return 0;
}
