CC=gcc
#CC=clang

DEBUG=_DEBUG_MORE
OPTIMZE=false
VERBOSE=true
NOW=$(shell date '+%y%m%d%H')
HOME=$(shell pwd | sed "s/trunk\/.*/trunk/")

CFLAGS=-Wall -Werror -fno-strict-aliasing -Wno-array-bounds -D_GNU_SOURCE
CXXFLAGS=-Wall -Werror -fno-strict-aliasing -Wno-array-bounds -D_GNU_SOURCE
ifneq ($(shell uname | grep "Darwin"),)
LDFLAGS=-lpthread -lm -lc
else
LDFLAGS=-Wl,-static -Wl,-call_shared -lpthread -lm -lc
endif

ifeq ($(CC), gcc)
	CXX=g++
endif

ifeq ($(CC), clang)
	CXX=clang++
endif

#ifdef CCACHE_HOME
#	CC:=ccache $(CC) $(XFLAGS)
#	CXX:=ccache $(CXX) $(XFLAGS)
#endif

ifneq ($(VERBOSE), true)
	CC:=@$(CC)
endif

ifdef DEBUG
	CFLAGS+=-D$(DEBUG)
	CXXFLAGS+=-D$(DEBUG)
endif

ifeq ($(OPTIMZE), true)
	CFLAGS+=-O2 -fvisibility=hidden
	CXXFLAGS+=-O2 -fvisibility=hidden
	LDFLAGS+=-O2
else
	CFLAGS+=-ggdb
	CXXFLAGS+=-ggdb
endif

ifneq ($(shell uname | grep "Cygwin"),)
	CFLAGS+=-D_KERNEL
	LDFLAGS+=-Wl,--stack,8388608
endif

BINS=server client
SRCS=$(wildcard *.c *.cc *.cpp)
OBJS=$(patsubst %.c,%.o,$(patsubst %.cc,%o,$(patsubst %.cpp,%.o,$(SRCS))))
BASEOBJS=gear.o

%.o:%.c
	$(CC) -o $@ $^ -c $(CFLAGS)

%.o:%.cc
	$(CC) -o $@ $^ -c $(CXXFLAGS)

%.o:%.cpp
	$(CXX) -o $@ $^ -c $(CXXFLAGS)

%:%.o $(BASEOBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)

all:$(OBJS) $(BINS)

clean:
	rm -f $(OBJS) $(BINS)

tags:
	@if [ -f $(HOME)/tags ]; then rm $(HOME)/tags; fi
ifeq ($(shell uname | grep "Darwin"),)
	ctags -f $(HOME)/tags -aR --c-types=+px $(HOME)/*
else
	ctags -f $(HOME)/tags -aw $(HOME)/*
endif

.PHONY:all release install clean tags

