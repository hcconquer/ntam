/*
 * server.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: hanchen
 */

#include <map>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <inttypes.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <getopt.h>
#include <libgen.h>
#include <fcntl.h>
#if ! defined(__APPLE__)
#include <endian.h>
#endif

#include "gear.h"

#include "CNP_Protocol1.h"

using namespace std;
using namespace cnp;

#define RUN_MODE_START   1
#define RUN_MODE_STOP    2
#define RUN_MODE_RESTART 3

#define USER_MAX            1024
#define SESSION_MAX         1024
#define TRADE_MAX           1024
#define LOGON_CLIENT_MAX    128

#define LOG_STATE_UN        0
#define LOG_STATE_ON        1
#define LOG_STATE_OFF       2

typedef struct {
	DWORD                   m_dwKey;

    char                    m_szFirstName[MAX_NAME_LEN];
    char                    m_szLastName[MAX_NAME_LEN];
    DWORD                   m_dwDLNumber;
    DWORD                   m_dwSSNumber;
    char                    m_szPIN[MAX_PIN_LEN];

    WORD                    m_wClientID;
    DWORD                   m_dwAmount;

    int                     log_state;
} user_t;

//typedef struct {
//	DWORD                   m_dwKey;
//
//	WORD                    m_wClientID;
//	DWORD                   m_dwUserKey;
//} session_t;

typedef struct {
	DWORD                   m_dwKey;

	DWORD                   m_dwUserKey;

    DWORD                   m_dwID;
    QWORD                   m_qwDateTime;
    DWORD                   m_dwAmount;
    WORD                    m_wType;
} trade_t;

typedef struct {
    int max;
    int num;
    user_t list[USER_MAX];
} user_list_t;

//typedef struct {
//    int max;
//    int num;
//    session_t list[USER_MAX];
//} session_list_t;

typedef struct {
    int max;
    int num;
    trade_t list[USER_MAX];
} trade_list_t;

typedef struct
{
    /* config */
    int  daemon;
    int  worker_num;

    in_addr_t loc_addr;
    int loc_port;

    /* context */
    int run_mode;
    char proc_name[128];
    int work;

    int loc_udp_sock;

    user_list_t *users;
//    session_list_t *sessions;
    trade_list_t *trades;

    DWORD despoit_amount;
    DWORD withdraw_amount;
    DWORD cash_amount;
    DWORD g_client_id;
    DWORD g_trade_id;

    char zeros[1]; // for some struct not need to memset

    map<pthread_t, int> worker_map;

    pthread_mutex_t db_lock;
} Config;

static Config config[1];

int Lock() {
	pthread_mutex_lock(&config->db_lock);
	return 0;
}

int UnLock() {
	pthread_mutex_unlock(&config->db_lock);
	return 0;
}

int UserCreate(const user_t &user) {
	user_list_t *users = config->users;
	user_t *p = NULL;
	int idx = 0;

	if (users->num >= users->max) {
        DEBUGS("user_num: %d, user_max: %d", users->num, users->max);
		return -1;
	}

	for (idx = 0; idx < users->max; idx++) {
		p = &users->list[idx];
		if (strlen(p->m_szFirstName) == 0) {
			break;
		}
	}

	DEBUGS("key: %d", idx);

	memcpy(p, &user, sizeof(*p));
	p->m_dwKey = idx;

	return 0;
}

int UserGetByFirstName(user_t **user, const char *szFirstName) {
	user_list_t *users = config->users;
	user_t *p = NULL;
	int idx = -1, found = 0;

	if (user == NULL) {
		return -1;
	}

	if (strlen(szFirstName) == 0) {
		return -2;
	}

	for (idx = 0; idx < users->max; idx++) {
		p = &users->list[idx];
		if (strcasecmp(p->m_szFirstName, szFirstName) == 0) {
			found = 1;
			break;
		}
	}

	if (!found) {
		return -2;
	}

	*user = p;

	return 0;
}

int UserGetByClientid(user_t **user, WORD wClientID) {
	user_list_t *users = config->users;
	user_t *p = NULL;
	int idx = -1, found = 0;

	if (user == NULL) {
		return -1;
	}

	for (idx = 0; idx < users->max; idx++) {
		p = &users->list[idx];
		if (p->m_wClientID == wClientID) {
			found = 1;
			break;
		}
	}

	if (!found) {
		return -2;
	}

	*user = p;

	return 0;
}

int UserGetByKey(user_t **user, DWORD dwKey) {
	user_list_t *users = config->users;
	user_t *p = NULL;

	if (user == NULL) {
		return -1;
	}

	p = &users->list[dwKey];
	if (STR_EMPTY(p->m_szFirstName)) {
		return -2;
	}

	*user = p;

	return 0;
}

//int SessionCreate(const session_t &session) {
//	session_list_t *sessions = config->sessions;
//	session_t *p = NULL;
//	int idx = 0;
//
//	if (sessions->num >= sessions->max) {
//        DEBUGS("user_num: %d, user_max: %d", sessions->num, sessions->max);
//		return -1;
//	}
//
//	for (idx = 0; idx < sessions->max; idx++) {
//		p = &sessions->list[idx];
//		if ((p->m_wClientID == 0) && (p->m_dwUserKey == 0)) {
//			break;
//		}
//	}
//
//	memcpy(p, &session, sizeof(*p));
//	p->m_dwKey = idx;
//
//	return 0;
//}

//int SessionGetByClient(session_t **session, WORD wClientID) {
//	session_list_t *sessions = config->sessions;
//	session_t *p = NULL;
//	int idx = -1, found = 0;
//
//	if (session == NULL) {
//		return -1;
//	}
//
//	for (idx = 0; idx < sessions->max; idx++) {
//		p = &sessions->list[idx];
//		if (p->m_wClientID <= 0) {
//			continue;
//		}
//		if (p->m_wClientID == wClientID) {
//			found = 1;
//			break;
//		}
//	}
//
//	if (!found) {
//		return -2;
//	}
//
//    *session = p;
//
//	return 0;
//}

//int SessionGetByUserKey(session_t **session, DWORD dwUserKey) {
//	session_list_t *sessions = config->sessions;
//	session_t *p = NULL;
//	int idx = -1, found = 0;
//
//	if (session == NULL) {
//		return -1;
//	}
//
//	for (idx = 0; idx < sessions->max; idx++) {
//		p = &sessions->list[idx];
//		if (p->m_wClientID <= 0) {
//			continue;
//		}
//		if (p->m_dwUserKey == dwUserKey) {
//			found = 1;
//			break;
//		}
//	}
//
//	if (!found) {
//		return -2;
//	}
//
//    *session = p;
//
//	return 0;
//}

//int SessionDelete(session_t *session) {
//	memset(session, 0, sizeof(*session));
//	return 0;
//}

int TradeCreate(const trade_t &trade) {
	trade_list_t *trades = config->trades;
	trade_t *p = NULL;
	int idx = 0;

	if (trades->num >= trades->max) {
        DEBUGS("user_num: %d, user_max: %d", trades->num, trades->max);
		return -1;
	}

	for (idx = 0; idx < trades->max; idx++) {
		p = &trades->list[idx];
		if (p->m_dwID == 0) {
			break;
		}
	}

	memcpy(p, &trade, sizeof(*p));
	p->m_dwKey = idx;

	return 0;
}

int TradeGet(trade_t *trade, DWORD dwUserKey, DWORD dwStart, WORD count) {
	trade_list_t *trades = config->trades;
	trade_t *p = NULL;
	int cnt = 0, i;

	if (trade == NULL) {
		return -1;
	}

	for (i = 0; i < trades->max ; i++) {
		p = &trades->list[i];
		if (p->m_qwDateTime == 0) {
			continue;
		}
		if (p->m_dwUserKey != dwUserKey) {
			continue;
		}
		if (p->m_dwID < dwStart) {
			continue;
		}
		memcpy(&trade[cnt], p, sizeof(*trade));
		cnt++;
		if (cnt >= count) {
			break;
		}
	}

	return cnt;
}

int Settle(user_t *user, int oper, int type, DWORD dwAmount) {
	DEBUGS("settle, oper: %d, type: %d, amount: %d, user_amount: %d",
			oper, type, dwAmount, user->m_dwAmount);

	if (oper == TT_DEPOSIT) {
		if (FUND_MAX - user->m_dwAmount < dwAmount) {
			DEBUGS("amount up to max");
			return CER_ERROR;
		}
		if (type == DT_CASH) {
			if (DESPOIT_MAX - config->despoit_amount < dwAmount) {
				DEBUGS("despoit full");
				return CER_ATM_FULL;
			}
			config->cash_amount += dwAmount;
			config->despoit_amount += dwAmount;
		}
		user->m_dwAmount += dwAmount;
	} else if (oper == TT_WITHDRAWAL) {
		if (user->m_dwAmount < dwAmount) {
			DEBUGS("not enough money, amount: %d", user->m_dwAmount);
			return CER_INSUFFICIENT_FUNDS;
		}
		if (type == DT_CASH) {
			if (WITHDRAW_MAX - config->withdraw_amount < dwAmount) {
				DEBUGS("withdraw full");
				return CER_ATM_ZERO;
			}
			if (dwAmount > config->cash_amount) {
				DEBUGS("not enough cash, cash: %d", config->cash_amount);
				return CER_INSUFFICIENT_CASH;
			}
			config->cash_amount -= dwAmount;
			config->withdraw_amount += dwAmount;
		}
		user->m_dwAmount -= dwAmount;
	}

	return 0;
}

/**
 * 1.restun server version and a auto-increment client_id
 */
static ssize_t HandleConnect(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_CONNECT_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;

	CNP_CONNECT_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	CONNECTION_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("connect");

	Lock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = CER_SUCCESS;
	sbody->m_wMajorVersion = g_wMajorVersion;
	sbody->m_wMinorVersion = g_wMinorVersion;
	sbody->m_wClientID = config->g_client_id;

	UnLock();

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the user is exist or not by first_name, if exist return error
 * 2.create with the request parameters
 * 3.return to the client
 */
static ssize_t HandleCreateAccount(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_CREATE_ACCOUNT_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	CREATE_ACCOUNT_REQUEST *rbody = &req->m_Request;

	CNP_CREATE_ACCOUNT_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	CREATE_ACCOUNT_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("create_account, first_name: %s, last_name: %s, pin: %s, dlnum: %d, ssnum: %d",
			rbody->m_szFirstName, rbody->m_szLastName, rbody->m_szPIN, rbody->m_dwDLNumber, rbody->m_dwSSNumber);

	int ret = 0, result = CER_SUCCESS;

	Lock();

	user_t *user, newuser;
//	ret = UserGetByClientid(&user, rhead->m_wClientID);
//	if (ret == 0) {
//		DEBUGS("clientid exist, cliendid: %d", rhead->m_wClientID);
//		result = CER_USER_EXIST;
//	}
	ret = UserGetByFirstName(&user, rbody->m_szFirstName);
	if (ret == 0) {
		DEBUGS("user exist, first_name: %s", rbody->m_szFirstName);
		result = CER_USER_EXIST;
	}

	if (result == CER_SUCCESS) {
		memset(&newuser, 0, sizeof(newuser));
		newuser.m_wClientID = config->g_client_id;
		strncpy_v(newuser.m_szFirstName, rbody->m_szFirstName, sizeof(newuser.m_szFirstName));
		strncpy_v(newuser.m_szLastName, rbody->m_szLastName, sizeof(newuser.m_szLastName));
		strncpy_v(newuser.m_szPIN, rbody->m_szPIN, sizeof(newuser.m_szPIN));
		newuser.log_state = LOG_STATE_UN;

		ret = UserCreate(newuser);
		if (ret < 0) {
			DEBUGS("user create fail, ret: %d", ret);
			result = CER_ERROR;
		}
	}

	if (result == CER_SUCCESS) {
		config->g_client_id++;
	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;
	sbody->m_wClientID = newuser.m_wClientID;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check user is exist, if not exist, return error
 * 2.check user's pin is match or not, if not macth, return error
 * 3.create a session, to record the user's logon state, which will be remove until logoff
 * 4.return to the client
 */
static ssize_t HandleLogon(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_LOGON_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	LOGON_REQUEST *rbody = &req->m_Request;

	CNP_LOGON_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	LOGON_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("logon, first_name: %s, pin: %s", rbody->m_szFirstName, rbody->m_szPIN);

	int ret = 0, result = CER_SUCCESS;

	Lock();

	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("clientid not exist, cliendid: %d", rhead->m_wClientID);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state == LOG_STATE_ON) {
			DEBUGS("client been logon");
			result = CER_CLIENT_BEEN_LOGGEDON;
		} else {
			if ((strcmp(user->m_szFirstName, user->m_szFirstName) != 0)
					|| (strcmp(user->m_szPIN, rbody->m_szPIN) != 0)) {
				DEBUGS("user pin mismatch, first_name: %s, pin: %s", user->m_szFirstName, user->m_szPIN);
				result = CER_INVALID_NAME_PIN;
			} else {
				user->log_state = LOG_STATE_ON;
			}
		}
	}

//	user_t *user = NULL;
//	ret = UserGetByFirstName(&user, rbody->m_szFirstName);
//	if (ret != 0) {
//		DEBUGS("user not exist, first_name: %s", rbody->m_szFirstName);
//		result = CER_AUTHENICATION_FAILED;
//	} else {
//		if (user->log_state == LOG_STATE_ON) {
//			DEBUGS("client been logon");
//			result = CER_CLIENT_BEEN_LOGGEDON;
//		} else {
//			if (strcmp(user->m_szPIN, rbody->m_szPIN) != 0) {
//				DEBUGS("user pin mismatch, first_name: %s", user->m_szFirstName);
//				result = CER_INVALID_NAME_PIN;
//			} else {
//				user->log_state = LOG_STATE_ON;
//			}
//		}
//	}

//	if (result == CER_SUCCESS) {
//		session_t *session;
//		ret = SessionGetByUserKey(&session, user->m_dwKey);
//		if (ret == 0) {
//			DEBUGS("client been logon, clientid: %d", session->m_wClientID);
//			result = CER_CLIENT_BEEN_LOGGEDON;
//		}
//	}
//
//	if (result == CER_SUCCESS) {
//		session_t newsess;
//		memset(&newsess, 0, sizeof(newsess));
//
//		newsess.m_wClientID = rhead->m_wClientID;
//		newsess.m_dwUserKey = user->m_dwKey;
//
//		ret = SessionCreate(newsess);
//		if (ret < 0) {
//			DEBUGS("user logon fail, first_name: %s", user->m_szFirstName);
//			result = CER_ERROR;
//		}
//	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the login session is exist, if not exist, user not logoned, return error
 * 2.remove the login session
 * 3.return to the client
 */
static ssize_t HandleLogoff(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_LOGOFF_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;

	CNP_LOGOFF_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	LOGOFF_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("logoff");

	int ret = 0, result = CER_SUCCESS;

	Lock();

	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("clientid not exist, cliendid: %d", rhead->m_wClientID);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_OFF) {
			user->log_state = LOG_STATE_OFF;
		} else {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		}
	}

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	} else {
//		DEBUGS("client: %d, user_key: %d", session->m_wClientID, session->m_dwUserKey);
//	}
//
//	if (result == CER_SUCCESS) {
//		SessionDelete(session);
//		DEBUGS("client: %d, user_key: %d", session->m_wClientID, session->m_dwUserKey);
//	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the session is exist, if not exist, return error
 * 2.get the user by session info
 * 3.try to deposit into user's account, if fail, return error
 * 4.if deposit success, save trade log
 * 5.return to the client
 */
static ssize_t HandleDeposit(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_DEPOSIT_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	DEPOSIT_REQUEST *rbody = &req->m_Request;

	CNP_DEPOSIT_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	DEPOSIT_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("deposit, amount: %d, type: %d", rbody->m_dwAmount, rbody->m_wType);

	int ret = 0, result = CER_SUCCESS;

	Lock();

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	}

//	user_t *user;

//	if (result == CER_SUCCESS) {
//		ret = UserGetByKey(&user, session->m_dwUserKey);
	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("user not exist, ret: %d", ret);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_ON) {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		}
	}

	DEBUGS("key: %d, first_name: %s, amount: %d", user->m_dwKey, user->m_szFirstName, user->m_dwAmount);

	ret = Settle(user, TT_DEPOSIT, rbody->m_wType, rbody->m_dwAmount);
	if (ret != 0) {
		DEBUGS("settle fail, ret: %d", ret);
		result = ret;
	}
//	}

	if (result == CER_SUCCESS) {
		trade_t trade;
		memset(&trade, 0, sizeof(trade));

		trade.m_dwUserKey = user->m_dwKey;
		trade.m_dwID = config->g_trade_id++;
		trade.m_dwAmount = rbody->m_dwAmount;
		trade.m_wType = TT_DEPOSIT;
		trade.m_qwDateTime = TimeGet(TIME_SEC);

		ret = TradeCreate(trade);
		if (ret < 0) {
			DEBUGS("trade create fail");
			result = CER_ERROR;
		}
	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the session is exist, if not exist, return error
 * 2.get the user by session info
 * 3.try to withdrawal into user's account, if fail, return error
 * 4.if withdrawal success, save trade log
 * 5.return to the client
 */
static ssize_t HandleWithdrawal(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_WITHDRAWAL_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	WITHDRAWAL_REQUEST *rbody = &req->m_Request;

	CNP_WITHDRAWAL_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	WITHDRAWAL_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("withdrawal, amount: %d", rbody->m_dwAmount);

	int ret = 0, result = CER_SUCCESS;

	Lock();

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	}

//	user_t *user;
//	if (result == CER_SUCCESS) {
//		ret = UserGetByKey(&user, session->m_dwUserKey);
	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("user not exist, ret: %d", ret);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_ON) {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		}
	}

	ret = Settle(user, TT_WITHDRAWAL, DT_CASH, rbody->m_dwAmount);
	if (ret != 0) {
		DEBUGS("settle fail, ret: %d", ret);
		result = ret;
	}
//	}

	if (result == CER_SUCCESS) {
		trade_t trade;
		memset(&trade, 0, sizeof(trade));

		trade.m_dwUserKey = user->m_dwKey;
		trade.m_dwID = config->g_trade_id++;
		trade.m_dwAmount = rbody->m_dwAmount;
		trade.m_wType = TT_WITHDRAWAL;
		trade.m_qwDateTime = TimeGet(TIME_SEC);

		ret = TradeCreate(trade);
		if (ret < 0) {
			DEBUGS("trade create fail");
			result = CER_ERROR;
		}
	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the session is exist, if not exist, return error
 * 2.get the user by session info
 * 3.return the user's account amount to the client
 */
static ssize_t HandleBalanceQuery(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_BALANCE_QUERY_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;

	CNP_BALANCE_QUERY_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	BALANCE_QUERY_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("balance_query");

	int ret = 0, result = CER_SUCCESS;

	Lock();

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	} else {
//		// DEBUGS("client: %d, user_key: %d", session->m_wClientID, session->m_dwUserKey);
//	}

//	user_t *user = NULL;
//	if (result == CER_SUCCESS) {
//		ret = UserGetByKey(&user, session->m_dwUserKey);
//		if (ret < 0) {
//			DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
//			result = CER_AUTHENICATION_FAILED;
//		}
//	}

	DWORD dwBalance = 0;

	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_ON) {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		} else {
			dwBalance = user->m_dwAmount;
		}
	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwBalance = dwBalance;
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * 1.check the session is exist, if not exist, return error
 * 2.get the user by session info
 * 3.get trade log by user and request parameters
 * 4.return the user's transaction list to the client
 */
static ssize_t HandleTransactionQuery(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_TRANSACTION_QUERY_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	TRANSACTION_QUERY_REQUEST *rbody = &req->m_Request;

	CNP_TRANSACTION_QUERY_RESPONSE *resp = (typeof(resp)) sbuf;
	CNP_STD_HDR *shead = &resp->m_Hdr;
	TRANSACTION_QUERY_RESPONSE *sbody = &resp->m_Response;

	DEBUGS("transaction_query, start: %d, count: %d", rbody->m_dwStartID, rbody->m_wTransactionCount);

	int ret = 0, result = CER_SUCCESS;

	Lock();

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	} else {
//		// DEBUGS("client: %d, user_key: %d", session->m_wClientID, session->m_dwUserKey);
//	}

//	user_t *user = NULL;
//	if (result == CER_SUCCESS) {
//		ret = UserGetByKey(&user, session->m_dwUserKey);
//		if (ret < 0) {
//			DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
//			result = CER_AUTHENICATION_FAILED;
//		}
//	}

	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_ON) {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		}
	}

	trade_t trades[TRADE_MAX];
	int tnum = 0;
	if (result == CER_SUCCESS) {
		memset(trades, 0, sizeof(trades));
		tnum = TradeGet(trades, user->m_dwKey, rbody->m_dwStartID, rbody->m_wTransactionCount);
	}

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;
	sbody->m_wTransactionCount = tnum;
	for (int i = 0; i < tnum; i++) {
		sbody->m_rgTransactions[i].m_dwID = trades[i].m_dwID;
		sbody->m_rgTransactions[i].m_qwDateTime = trades[i].m_qwDateTime;
		sbody->m_rgTransactions[i].m_dwAmount = trades[i].m_dwAmount;
		sbody->m_rgTransactions[i].m_wType = trades[i].m_wType;
	}

	UnLock();

	slen = sizeof(*resp) + tnum * sizeof(sbody->m_rgTransactions[0]);
	shead->m_wDataLen = slen;

	return slen;
}

/*
 * 1.check the session is exist, if not exist, return error
 * 2.get the user by session info
 * 3.withdrawal $10 from user account
 * 3.return to the client
 */
static ssize_t HandleStampPurchase(void *sbuf, size_t slen, const void *rbuf, size_t rlen) {
	CNP_STAMP_PURCHASE_REQUEST *req = (typeof(req)) rbuf;
	CNP_STD_HDR *rhead = &req->m_Hdr;
	STAMP_PURCHASE_REQUEST *rbody = &req->m_Request;

	CNP_STAMP_PURCHASE_RESPONSE resp;
	CNP_STD_HDR *shead = &resp.m_Hdr;
	STAMP_PURCHASE_RESPONSE *sbody = &resp.m_Response;

	DEBUGS("stamp_purchase, amount: %d", rbody->m_dwAmount);

	int ret = 0, result = CER_SUCCESS;

	Lock();

//	session_t *session;
//	ret = SessionGetByClient(&session, rhead->m_wClientID);
//	if (ret < 0) {
//		DEBUGS("client not logon");
//		result = CER_CLIENT_NOT_LOGGEDON;
//	} else {
//		// DEBUGS("client: %d, user_key: %d", session->m_wClientID, session->m_dwUserKey);
//	}
//
//	user_t *user = NULL;
//	if (result == CER_SUCCESS) {
//		ret = UserGetByKey(&user, session->m_dwUserKey);
//		if (ret < 0) {
//			DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
//			result = CER_AUTHENICATION_FAILED;
//		}
//	}

	user_t *user = NULL;
	ret = UserGetByClientid(&user, rhead->m_wClientID);
	if (ret != 0) {
		DEBUGS("user not exist, first_name: %s", user->m_szFirstName);
		result = CER_AUTHENICATION_FAILED;
	} else {
		if (user->log_state != LOG_STATE_ON) {
			DEBUGS("client not logon");
			result = CER_CLIENT_NOT_LOGGEDON;
		}
	}

	DWORD dwFee = 0;
	if (result == CER_SUCCESS) {
		dwFee = rbody->m_dwAmount * STAMP_FEE;
		ret = Settle(user, TT_WITHDRAWAL, DT_CHECK, dwFee);
		if (ret != 0) {
			DEBUGS("settle fail, ret: %d", ret);
			result = ret;
		}
	}

	if (result == CER_SUCCESS) {
		trade_t trade;
		memset(&trade, 0, sizeof(trade));

		trade.m_dwUserKey = user->m_dwKey;
		trade.m_dwID = config->g_trade_id++;
		trade.m_dwAmount = dwFee;
		trade.m_wType = TT_WITHDRAWAL;
		trade.m_qwDateTime = TimeGet(TIME_SEC);

		ret = TradeCreate(trade);
		if (ret < 0) {
			DEBUGS("trade create fail");
			result = CER_ERROR;
		}
	}

	UnLock();

	memcpy(shead, rhead, sizeof(*shead));
	sbody->m_dwResult = result;

	slen = sizeof(resp);
	shead->m_wDataLen = slen;

	memcpy(sbuf, &resp, slen);

	return slen;
}

/*
 * handle one request, receive the request from client,
 * parese the request_id from head, and assign to special handle
 */
static int HandleUdpRequest(int sock) {
    static int_ptr_pair_t handles[] = {
    		{ CNP_CONNECT_REQUEST_ID,           (void *) HandleConnect },
			{ CNP_CREATE_ACCOUNT_REQUEST_ID,    (void *) HandleCreateAccount },
			{ CNP_LOGON_REQUEST_ID,             (void *) HandleLogon },
    		{ CNP_LOGOFF_REQUEST_ID,            (void *) HandleLogoff },
			{ CNP_DEPOSIT_REQUEST_ID,           (void *) HandleDeposit },
			{ CNP_WITHDRAWAL_REQUEST_ID,        (void *) HandleWithdrawal },
			{ CNP_BALANCE_QUERY_REQUEST_ID,     (void *) HandleBalanceQuery },
			{ CNP_TRANSACTION_QUERY_REQUEST_ID, (void *) HandleTransactionQuery },
			{ CNP_PURCHASE_STAMPS_REQUEST_ID,   (void *) HandleStampPurchase },
    };

    char rbuf[65536], sbuf[65536];
	ssize_t rlen = 0, slen = 0, len = 0;
    memset(rbuf, 0, sizeof(rbuf));

    struct sockaddr_in cltaddr;
    socklen_t cltaddrlen = sizeof(cltaddr);

    rlen = recvfrom(sock, rbuf, sizeof(rbuf), 0, (struct sockaddr *) &cltaddr, &cltaddrlen);

    if (rlen < (ssize_t) sizeof(CNP_STD_HDR)) {
    	return -1;
    }

    CNP_STD_HDR *head = (typeof(head)) rbuf;
    DWORD msgtype = head->m_dwMsgType;

    int hidx = MAP_GET(handles, (int) msgtype);
	if (hidx < 0) {
	    DEBUGS("msgtype not found: %d", msgtype);
		return -2;
	}

	int_ptr_pair_t *handle = &handles[hidx];
	ssize_t (*func)(void *, size_t, void *, size_t) = (typeof(func)) handle->value;

	memset(sbuf, 0, sizeof(sbuf));
	slen = func(sbuf, sizeof(sbuf), rbuf, rlen);

    len = sendto(sock, sbuf, slen, 0, (struct sockaddr *) &cltaddr, cltaddrlen);
    if (len < 0) {
    	DEBUGS("msytype: %d, slen: %zd, len: %zd", msgtype, slen, len);
    }

    return 0;
}

/*
 * main work, endless loop, wait client's request and handle it
 */
static void *HandleWork(void *arg) {
	Config *config = (typeof(config)) arg;
	int ret = 0;

    while (!g_terminated) {
        if (config->work) {
            break;
        }
        msleep(10);
    }

	while (!g_terminated) {
		ret = WaitForFd(config->loc_udp_sock, FD_EV_RD, -1, 0);
		if (ret > 0) {
            ret = HandleUdpRequest(config->loc_udp_sock);
		}
	}

	return 0;
}

/*
 * parse the params from command line
 */
static int InitArgs(void *arg, int argc, char **argv) {
    static struct option long_options[] = {
        { "help", no_argument, NULL, 'h' },
        { "start", no_argument, NULL, 's' },
        { "stop", no_argument, NULL, 'p' },
        { "restart", no_argument, NULL, 'r' },
        { NULL, 0, NULL, 0 }
    };
    Config *config = (typeof(config)) arg;
    int ret = 0, optv = 0, option_index = 0;

    strncpy_v(config->proc_name, argv[0], sizeof(config->proc_name));

    if (argc < 2)
    {
        return -1;
    }

    while (1)
    {
        optv = getopt_long(argc, argv, "hspr", long_options, &option_index);
        if (optv < 0)
        {
            break;
        }
        switch (optv)
        {
        case 'h':
            ret = 1;
            break;
        case 's':
            config->run_mode = RUN_MODE_START;
            break;
        case 'p':
            config->run_mode = RUN_MODE_STOP;
            break;
        case 'r':
            config->run_mode = RUN_MODE_RESTART;
            break;
        default:
            ret = -1;
            break;
        }
        if (ret != 0)
        {
            break;
        }
    }

    return ret;
}

static int InitProc(void *arg)
{
	Config *config = (typeof(config)) arg;
    int ret = 0, pnum = 0;
    pid_t pids[256];

    sig_reg(terminate, SIGINT, SIGQUIT, SIGTERM, 0);
    sig_reg(SIG_IGN, SIGHUP, 0);

    set_core_size(100 * 1024 * 1024);

    pnum = get_pids_by_name(pids, ARRLEN(pids), basename(config->proc_name));
    if (config->run_mode == RUN_MODE_START) {
        if (pnum > 0) {
            printf("been running with pid[%d]\n", pids[0]);
            return -1;
        }
        printf("start\n");
    } else if (config->run_mode == RUN_MODE_STOP) {
    	kill_procs(pids, pnum, SIGTERM, 1);
        printf("stop\n");
        return 1;
    } else if (config->run_mode == RUN_MODE_RESTART) {
    	ret = kill_procs(pids, pnum, SIGTERM, 1);
    	if (ret != 0) {
    		return -1;
    	}
        printf("restart\n");
    } else {
        return -1;
    }

    if (config->daemon > 0) {
        daemonize(NULL, 0);
    }

    return 0;
}

static int InitConfig(void *arg)
{
	Config *config = (typeof(config)) arg;

	/*
    eth_addr_t eth;
    memset(&eth, 0, sizeof(eth));
    GetLanEths(&eth, 1, NULL);
    DEBUGS("loc_addr: %s", inet_ntoa(*(struct in_addr * ) &eth.addr));
    */

    config->loc_addr = INADDR_ANY;
    config->loc_port = 5001;

    DEBUGS("loc_addr: %s", inet_ntoa(*(struct in_addr * ) &config->loc_addr));

    if (config->worker_num <= 0) {
    	config->worker_num = 4;
    }

//    if (config->g_client_id <= 0) {
//    	config->g_client_id = TimeGet(TIME_MS);
//    }

    return 0;
}

int InitNet(void *arg)
{
	int sock;

	sock = CreateSock(config->loc_addr, config->loc_port, INADDR_ANY, -1, SOCK_DGRAM, -1);
	if (sock < 0) {
		DEBUGS("create socket fail, result: %d, port: %d", sock, config->loc_port);
		return -1;
	}

    SetSockBlock(sock, 0);

	config->loc_udp_sock = sock;

    return 0;
}

/*
 * load or create db file and map to mem ptr
 * so program can read ptr as file
 */
static int InitMemDb(void **shm, const char *filepath, size_t size) {
	void *ptr = NULL;
	int fd, ret;
    size_t fz;

	fd = open(filepath, O_RDWR | O_CREAT, 0666);
	if (fd < 0) {
		DEBUGS("open file fail, ret: %d, filepath: %s, errno: %d", fd, filepath, errno);
		return -3;
	}

    fz = fsize(fd);
    if (fz < size) {
        ret = ftruncate(fd, size);
        if (ret < 0) {
            DEBUGS("reset file size fail, ret: %d, errno: %d", ret, errno);
            return -4;
        }
    }

	ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED) {
		DEBUGS("map file fail, errno: %d", errno);
		return -5;
	}

	*shm = ptr;

    if (fz < size) {
        return 1;
    }

	return 0;
}

/*
 * load user, session, trade file
 */
static int InitData(void *arg) {
	Config *config = (typeof(config)) arg;

	int ret = 0, i;
	size_t size = 0;

	ret = pthread_mutex_init(&config->db_lock, NULL);
	if (ret != 0) {
		DEBUGS("init db lock fail");
		return -1;
	}

    user_list_t *users = config->users;
	size = sizeof(*users);
	// ret = get_shm((void **) &users, 0x610001, size, 0666 | IPC_CREAT);
	ret = InitMemDb((void **) &users, "user.db", size);
	if (ret > 0) {
		memset(users, 0, size);
        users->num = 0;
        users->max = USER_MAX;
	} else if (ret < 0) {
		DEBUGS("init user memdb fail, ret: %d", ret);
		return -2;
	}
    config->users = users;

//	session_list_t *sessions = config->sessions;
//	size = sizeof(*sessions);
//	// ret = get_shm((void **) &sessions, 0x610002, size, 0666 | IPC_CREAT);
//	ret = InitMemDb((void **) &sessions, "session.db", size);
//	if (ret >= 0) { // create or new need to init
//		memset(sessions, 0, size);
//		sessions->num = 0;
//		sessions->max = SESSION_MAX;
//	} else if (ret < 0) {
//		DEBUGS("init session memdb fail, ret: %d", ret);
//		return -3;
//	}
//	config->sessions = sessions;

	trade_list_t *trades = config->trades;
	size = sizeof(*trades);
	// ret = get_shm((void **) &trades, 0x610003, size, 0666 | IPC_CREAT);
	ret = InitMemDb((void **) &trades, "trades.db", size);
	if (ret > 0) {
		memset(trades, 0, size);
		trades->num = 0;
		trades->max = TRADE_MAX;
	} else if (ret < 0) {
		DEBUGS("init trade memdb fail, ret: %d", ret);
		return -4;
	}
	config->trades = trades;

	for (i = 0; i < users->max; i++) {
		if (config->g_client_id <= users->list[i].m_wClientID) {
			config->g_client_id = users->list[i].m_wClientID + 1;
		}
	}

	for (i = 0; i < trades->max; i++) {
		if (config->g_trade_id <= trades->list[i].m_dwID) {
			config->g_trade_id = trades->list[i].m_dwID + 1;
		}
	}

    return 0;
}

static int Inits(void *arg) {
    const ptr_pair_t ifuncs[] = {
        { (void *) InitConfig,  (void *) "InitConfig" },
        { (void *) InitProc,    (void *) "InitProc" },
        { (void *) InitNet,     (void *) "InitNet" },
		{ (void *) InitData,    (void *) "InitData" },
    };

    Config *config = (typeof(config)) arg;
    int ret = 0, i = 0;
    int (*ifunc)(Config *) = NULL;
    char *desc = NULL;
    for (i = 0; i < (int) ARRLEN(ifuncs); i++) {
        ifunc = (typeof(ifunc)) ifuncs[i].key;
        desc = (typeof(desc)) ifuncs[i].value;
        ret = ifunc(config);
        if (ret < 0) {
            printf("%s fail, ret: %d\n", desc, ret);
            return -1;
        } else if (ret > 0) {
            return 1;
        }
    }

    return 0;
}

static int PrintUsage(int argc, char **argv) {
    printf("Usage: %s [OPTION]...\n", argv[0]);
    printf("  %3s %-10s %s\n", "-s,", "--start",           "start");
    printf("  %3s %-10s %s\n", "-p,", "--stop",            "stop");
    printf("  %3s %-10s %s\n", "-r,", "--restart",         "restart");
    printf("  %3s %-10s %s\n", "-h,", "--help",            "help");
    return 0;
}

int main(int argc, char **argv)
{
    int ret = 0;

    memset(config, 0, OFFSETOF(*config, zeros));

    ret = InitArgs(config, argc, argv);
    if (ret < 0) {
        PrintUsage(argc, argv);
        return -1;
    } else if (ret == 1) { // help
        PrintUsage(argc, argv);
        return 0;
    }

    ret = Inits(config);
    if (ret < 0) { // init fail
        return -2;
    } else if (ret > 0) { // init stop
    	exit(0);
    }

    DEBUGS("start");

	pthread_t *workers = (pthread_t *) malloc(config->worker_num * sizeof(pthread_t));
	for (int i = 0; i < config->worker_num; i++) {
		ret = pthread_create(&workers[i], NULL, HandleWork, config);
		if (ret < 0) {
			printf("start worker[%d] fail, ret: %d\n", i, ret);
			return -1;
		}
		config->worker_map.insert(pair<pthread_t, int>(workers[i], i));
	}

	config->work = 1; // start to work

    while (!g_terminated) {
    	for (int i = 0; i < config->worker_num; i++) {
    		pthread_join(workers[i], NULL);
    	}
    }

	DEBUGS("exit");

	return 0;
}
