/*===============================================================                          
 * gear.c
 * Created on: 2010-09-14
 * Modify    : 2015-03-23
 * Version   : 11056
 *     Author: chenhan
**===============================================================*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <float.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <assert.h>
#include <sched.h>
#include <dirent.h>
#include <signal.h>
#include <limits.h> 
#include <sys/types.h>
#include <sys/param.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <libgen.h>

#if defined(__APPLE__)
#include <sys/sysctl.h>
#endif

#include "gear.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************CORE*******************************/
char *strncpy_v(char *dst, const char *src, size_t num) {
    num = num > 0 ? num - 1 : 0;
    dst[num] = '\0';
    return strncpy(dst, src, num);
}

off_t filesize(char *filepath) {
	off_t len = -1;
	int fd = -1;
	if (filepath == NULL) {
        return -1;
    }
    if ((fd = open(filepath, O_RDONLY)) > 0) {
        len = fsize(fd);
        close(fd);
    }
    return len;
}

off_t fsize(int fd) {
    struct stat st;
    if (fstat(fd, &st) != 0) {
        return -1;
    }
    return st.st_size;
}

WEAK_FUNC_1(msleep, _, void, int msec) {
	usleep(msec * 1000);
}

ALIAS_FUNC_1(msleep, _msleep, void, int);

int daemonize(const char *dir, int noclose) {
    int fd, ret;

    umask(0);

    if (fork()) {
        exit(0);
    }

    if (setsid() == -1) {
        return -3;
    }

    if (!noclose) {
		for(fd = 3; fd < NOFILE; fd++) {
			close(fd);
		}
    }

    if ((dir != NULL) && (strlen(dir) > 0)) {
        ret = chdir(dir);
        if (ret < 0) {
            return -4;
        }
    }

    return 0;
}

static int set_limit(int type, int size) {
    int ret = 0;
   	struct rlimit limit;

	ret = getrlimit(type, &limit);
    if (ret < 0) {
        return -1;
    }

    if (size < 0) {
	    limit.rlim_cur = limit.rlim_max;
    } else {
	    limit.rlim_cur = size;
    }

	ret = setrlimit(type, &limit);
    if (ret < 0) {
        return -1;
    }

	return 0;
}

int set_core_dump() {
	return set_core_size(-1);
}

int set_core_size(int size) {
	return set_limit(RLIMIT_CORE, size);
}

int set_stack_size(int size) {
	return set_limit(RLIMIT_STACK, size);
}

int get_cpu_num() {
	return (int) sysconf(_SC_NPROCESSORS_CONF);
}

int get_pids_by_name(pid_t *pids, int max, const char *name) {
#if defined(__linux__)
    pid_t pid = -1, cpid = -1;
    DIR *dir = NULL;
    struct dirent *d = NULL;
    char filename[FILENAME_MAX], line[256];
    char pname[256], *p = NULL;
    FILE *fp = NULL;
    int num = 0;

    cpid = getpid();
    if (cpid < 0) {
    	return -2;
    }

    dir = opendir("/proc");
    if (dir == NULL) {
        return -3;
    }
    while ((d = readdir(dir)) != NULL) {
        pid = (typeof(pid)) strtoul(d->d_name, NULL, 0);
        if ((pid <= 0) || (pid == cpid)) { // avoid current
            continue;
        }
        snprintf(filename, sizeof(filename), "/proc/%s/status", d->d_name);
        fp = fopen(filename, "r");
        if (fp == NULL) {
            continue;
        }
        p = fgets(line, sizeof(line) - 1, fp);
        fclose(fp);
        if (p == NULL) {
            continue;
        }
        // line contain a string like "Name: binary_name" 
        sscanf(line, "%*s %s", pname);
        if (name != NULL) {
			if (strcmp(pname, name) != 0) {
				continue;
			}
        }
        if (pids != NULL) {
            *pids++ = pid;
        }
        num++;
        if ((max >= 0) && (num >= max)) {
        	break;
        }
    }
    closedir(dir);

    return num;
#elif defined(__APPLE__)
    static const int names[] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };
    int num = 0, ret = 0, pnum = 0, i = 0;
    struct kinfo_proc *procs = NULL;
    size_t length;
    pid_t cpid = -1;

    cpid = getpid();
    if (cpid < 0) {
    	return -2;
    }

	ret = sysctl((int *) names, (sizeof(names) / sizeof(*names)) - 1, NULL, &length, NULL, 0);
	if (ret < 0) {
		return -3;
	}

	procs = malloc(length);
	if (procs == NULL) {
		return -4;
	}

	ret = sysctl((int *) names, (sizeof(names) / sizeof(*names)) - 1, procs, &length, NULL, 0);
	if (ret < 0) {
		free(procs);
		return -5;
	}

	pnum = length / sizeof(struct kinfo_proc);
	for (i = 0; i < pnum; i++) {
		if (procs[i].kp_proc.p_pid == cpid) {
			continue;
		}
        if (name != NULL) {
    		if (strcmp(procs[i].kp_proc.p_comm, name) != 0) {
    			continue;
    		}
        }
		// DEBUGS("%s, %s", procs[i].kp_proc.p_comm, name);
		if (pids != NULL) {
			*pids++ = procs[i].kp_proc.p_pid;
		}
		num++;
        if ((max >= 0) && (num >= max)) {
        	break;
        }
	}

	free(procs);

	return num;
#else
	return -1;
#endif
}

/*
 * notice pidmap should be save data by 1,
 * pid == 0, is current process group
 * pid == -1, is perm can send signal, but not include process init(pid == 1)
 * pid < -1, is current process group and pid is (-pid)
**/
int kill_procs(pid_t *pids, int num, int signo, int wait) {
	pid_t pidmap[num];
	int ret = 0, eperm = 0, exist = 0, i;

	memcpy(pidmap, pids, num * sizeof(pid_t));

	for (;;) {
		for (i = 0; i < num; i++) {
			ret = kill(pids[i], signo);
			if (ret < 0) {
				if (errno == EINVAL) { // invalid signo
					return -1;
				} else if (errno == EPERM) { // no auth to kill
					eperm = 1;
					pidmap[i] = 1;
				} else if (errno == ESRCH) { // pid not exist, it's ok
					pidmap[i] = 1;
				}
			}
		}

		if (!wait) { // not wait
			return 0;
		}

		exist = 0;
		for (i = 0; i < num; i++) {
			if (pidmap[i] != 1) { // exist
				exist = 1;
			}
		}
		if (!exist) { // no pid exist
			if (eperm) {
				return 1;
			} else {
				return 0;
			}
		}

		if (g_terminated) { // current process killed, avoid unless loop
			return 1; // kill but not all
		}

        /* first loop is real signo 
         * the next is check process only
         * just kill(pid, 0)
        **/
        if (signo != 0) {
            signo = 0;
        }
	}

	return 0;
}

WEAK_FUNC_1(terminate, _, void, int signo) {
    DEBUGS("pid: %d, signo: %d", getpid(), signo);
	switch(signo) {
	default:
		g_terminated = 1; // program will be exit when next call loop
		break;
	}
}

ALIAS_FUNC_1(terminate, _terminate, void, int);

int sig_ignore(int sig) {
	return sig_reg(SIG_IGN, sig, 0);
}

WEAK_FUNC_0(RegSignal, _, void) {
    sig_reg(terminate, SIGINT, SIGQUIT, SIGTERM, 0);
    sig_reg(SIG_IGN, SIGHUP, 0);
}

ALIAS_FUNC_0(RegSignal, _RegSignal, void);

int sig_reg_s(void (*handler)(int), int *sigs, int num) {
	struct sigaction act;
    int i;

    if ((sigs == NULL) || (num < 0)) {
        return -1;
    }

    memset(&act, 0, sizeof(act));
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_RESTART;
	act.sa_handler = handler;

    for (i = 0; i < num; i++) {
	    sigaction(sigs[i], &act, NULL);
    }

    return 0;
}

int sig_reg(void (*handler)(int), ...) {
    int sig, sigs[256], num = 0;
	va_list ap;

	va_start(ap, handler);
    while (num < (int) ARRLEN(sigs)) {
        sig = va_arg(ap, int);
        if (sig <= 0 || sig >= 256) {
            break;
        }
        sigs[num] = sig;
        num++;
    }
	va_end(ap);
    
    return sig_reg_s(handler, sigs, num);
}


/********************************SEQUENCE*******************************/
int snprintfxxd(char *str, size_t strlen, const void *buf, int len) {
	size_t slen = 0;
	const unsigned char *s = (typeof(s)) buf;
	int i = 0;

	slen += snprintf(OFFOF(str, slen), strlen - slen, "        "); // 8 blanks
	for (i = 0; i < 16; i += 2) {
		slen += snprintf(OFFOF(str, slen), strlen - slen, "%02x%02x ", s[i], s[i + 1]);
		// DEBUGS("%zd, %02x%02x", slen, s[i], s[i + 1]);
	}
	slen += snprintf(OFFOF(str, slen), strlen - slen, " "); // 1 blanks
	// slen = 41;
	for (i = 0; i < 16; i++) {
		// DEBUGS("%d %c %d", s[i], s[i], isgraph(s[i]));
		if (isgraph(s[i])) {
			slen += snprintf(OFFOF(str, slen), strlen - slen, "%c", s[i]);
		} else {
			slen += snprintf(OFFOF(str, slen), strlen - slen, ".");
		}
	}

    return slen;
}


/********************************IO*******************************/
int WaitForFd(int fd, int ev, long msec, ...) {
	fd_set rfds, wfds, efds;
	int num, rev = 0;
	struct timeval tv;

    if (msec < 0) {
        msec = 0;
    }

    tv.tv_sec = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;

    FD_ZERO(&rfds);
    if (ev & FD_EV_RD) {
        FD_SET(fd, &rfds);
    }

    FD_ZERO(&wfds);
    if (ev & FD_EV_WR) {
        FD_SET(fd, &wfds);
    }

    FD_ZERO(&efds);
    if (ev & FD_EV_ERR) {
        FD_SET(fd, &efds);
    }

    num = select(fd + 1, &rfds, &wfds, &efds, &tv);
    if (num < 0) {
        return -1;
    }

    if (FD_ISSET(fd, &rfds)) {
        rev = rev | FD_EV_RD;
    }

    if (FD_ISSET(fd, &wfds)) {
        rev = rev | FD_EV_WR;
    }

    if (FD_ISSET(fd, &efds)) {
        rev = rev | FD_EV_ERR;
    }

    // DEBUGS("ev: %d, rev: %d", ev, rev);

	return rev;
}

ssize_t ReadEx(int fd, void *buf, size_t len, long timeo, int *flags, ...) {
	void *p = buf;	
	ssize_t n = 0;
    size_t sum = 0;
    uint32_t errs[32]; // 256 bit
    int err, po = 0;
    __attribute__ ((unused)) int bv;
	va_list ap;
    struct timeval s_tv, e_tv;
    long tm, left = timeo;
    int socktype, tms = 0, flgs = 0;

	va_start(ap, flags);
    while (1) {
        err = va_arg(ap, int);
        if (err <= 0 || err >= 256) {
            break;
        }
        bv = BITMAP_SET((void *) errs, 1, err, 0, 1);
    }
	va_end(ap);

    socktype = GetSockType(fd);

    if (timeo >= 0) {
        gettimeofday(&s_tv, NULL);
    }

	while (sum < len) {
        if (timeo >= 0) { // if check timeout
            gettimeofday(&e_tv, NULL);
            tm = TimeDiff(&e_tv, &s_tv);
            if (tm < 0) { // avoid cpu in different time
                tm = 0;
            }
            left = timeo - tm;
        }

        /*
         * check tms make sure at least 1 times check socket
         */
        if ((timeo >= 0) && (left < 0) && (tms > 0)) {
            break;
        }

        tms++;

        po = WaitForFd(fd, FD_EV_RD, left);
        if (po <= 0) { // timeout
            if (timeo >= 0) { // check timeout
                break;
            } else { // don't check timeout
                continue;
            }
        }

		n = read(fd, p, len);
		if (n > 0) {
            p = OFFOF(p, n);
			sum += n;
		} else if (n == 0) { // EOF
			flgs = FD_FLAG_EOF;
			break;
		} else {
            err = BITMAP_GET((void *) errs, 1, errno, 0);
            if (err == 0) { // not mask
                break;
            }
		}

		if (socktype == SOCK_DGRAM) {
			break;
		}
	}

    if (sum <= 0) { // read no data
        if ((po < 0) || (n < 0)) { // first read fail
            return -1;
        }
    }

    if (flags != NULL) {
    	*flags = flgs;
    }

    return sum;
}

ssize_t WriteEx(int fd, const void *buf, size_t len, long timeo, int *flags, ...) {
	void *p = (void *) buf;	
	ssize_t n = 0;
    size_t sum = 0;
    uint32_t errs[32]; // 256 bit
    int err, po = 0;
    __attribute__ ((unused)) int bv = 0; // only avoid warn in clang
	va_list ap;
    struct timeval s_tv, e_tv;
    long tm, left = 1000;
    int socktype, tms = 0, flgs = 0;

	va_start(ap, flags);
    while (1) {
        err = va_arg(ap, int);
        if (err <= 0 || err >= 256) {
            break;
        }
        bv = BITMAP_SET((void *) errs, 1, err, 0, 1);
    }
	va_end(ap);

    socktype = GetSockType(fd);

    if (timeo >= 0) {
        gettimeofday(&s_tv, NULL);
    }

	while (sum < len) {
        if (timeo >= 0) { // if check timeout
            gettimeofday(&e_tv, NULL);
            tm = TimeDiff(&e_tv, &s_tv);
            left = timeo - tm;
        }

        if ((timeo >= 0) && (left < 0) && (tms > 0)) {
            break;
        }

        tms++;

        po = WaitForFd(fd, FD_EV_WR, left);
        // DEBUGS("po: %d", po);
        if (po <= 0) { // timeout
            if (timeo >= 0) { // check timeout
                break;
            } else { // don't check timeout
                continue;
            }
        }

		n = write(fd, p, len);
        // DEBUGS("ret: %zd, errno: %d", n, errno);
		if (n > 0) {
            p = OFFOF(p, n);
			sum += n;
		} else {
            err = BITMAP_GET((void *) errs, 1, errno, 0);
            if (err == 0) { // not mask
                break;
            }
		}

		if (socktype == SOCK_DGRAM) {
			break;
		}
	}

    // DEBUGS("%zd, %zd", sum, n);
    if (sum <= 0) { // write no data
        if (n < 0) { // first write fail
            return -1;
        }
    }

    if (flags != NULL) {
    	*flags = flgs;
    }

	return sum;
}


/*****************************SOCKET****************************/
int IsResvEth(const eth_addr_t *eth) {
    static const char resv_addrs[][20] = {
        "0.0.0.0", "10.0.0.0", "127.0.0.1", "169.254.0.0",
        "172.16.0.0", "192.0.0.0", "192.0.2.0", "192.88.99.0",
        "192.168.0.0", "198.18.0.0", "198.51.100.0", "203.0.113.0",
        "224.0.0.0", "240.0.0.0", "255.255.255.255"
    };
    static const char resv_brdaddrs[][20] = {
        "0.0.0.0", "255.255.255.255"
    };
    int i;

    for (i = 0; i < (int) ARRLEN(resv_addrs); i++) {
        if (inet_addr(resv_addrs[i]) == eth->addr) {
            return 1; // true
        }
    }

    for (i = 0; i < (int) ARRLEN(resv_brdaddrs); i++) {
        if (inet_addr(resv_brdaddrs[i]) == eth->broadaddr) {
            return 1; // true
        }
    }

    return 0;
}

int IsLanEth(const eth_addr_t *eth) {
	uint8_t cNet;

    // DEBUGXXD((void *) &eth->addr, sizeof(eth->addr));
	cNet = (uint8_t) (eth->addr & 0xff); // cut off
	if (cNet == 10 || cNet == 128 || cNet == 172 || cNet == 192) {
		return 1; // true
	}

	return 0; // false
}

int GetLanEths(eth_addr_t *eth, int num, const char *name) {
    eth_addr_t es[32];
	int i, n = 0, en;

	en = GetEths(es, ARRLEN(es), name);
	// DEBUGS("eth_num: %d", en);
	if (en <= 0) {
		return -1;
	}

	for (i = 0; i < en; i++) {
        // DEBUGS("%s", inet_ntoa(*(struct in_addr * ) &es[i].addr));
        if (n >= num) {
            break;
        }
		if (IsResvEth(&es[i])) {
			continue;
		}
		if (IsLanEth(&es[i])) {
            eth[n++] = es[i];
		}
	}

	return n;
}

int GetWanEths(eth_addr_t *eth, int num, const char *name) {
    eth_addr_t es[32];
	int i, n = 0, en;

	en = GetEths(es, ARRLEN(es), NULL);
	if (en <= 0) {
		return -1;
	}

	for (i = 0; i < en; i++) {
        if (n >= num) {
            break;
        }
		if (IsResvEth(&es[i])) {
			continue;
		}
		if (!IsLanEth(&es[i])) {
            eth[n++] = es[i];
		}
	}

	return n;
}

/**
 * eths, the eths match with name
 * num,  the num of eths to get
 * name, the eth name to get, if NULL, match all eths
 */
int GetEths(eth_addr_t *eths, int num, const char *name) {
	struct ifconf ifc;
	struct ifreq *ifr, ifreq;
	char ifcb[65536], pname[256];
	eth_addr_t eth;
	int sock = -1, ret, cnt = 0;
	size_t off = 0, len = 0;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		return -1;
	}

	memset(ifcb, 0, sizeof(ifcb));
	memset(&ifc, 0, sizeof(ifc));
	ifc.ifc_len = sizeof(ifcb);
	ifc.ifc_buf = (caddr_t) ifcb;

	ret = ioctl(sock, SIOCGIFCONF, (char *) &ifc);
	if (ret < 0) {
		DEBUGS("ioctl fail, ret: %d", ret);
		close(sock);
		return -2;
	}

	memset(pname, 0, sizeof(pname));
	while (off < ifc.ifc_len) {
		ifr = OFFOF(ifc.ifc_req, off);

#if defined(__APPLE__)
		len = IFNAMSIZ + ifr->ifr_addr.sa_len;
#else
		len = sizeof(*ifr);
#endif

		off += len;

		if (strcmp(pname, ifr->ifr_name) == 0) {
			continue;
		}

		// DEBUGS("%s", ifr->ifr_name);

		strncpy_v(pname, ifr->ifr_name, sizeof(pname));

        if ((name != NULL) && (strcmp(ifr->ifr_name, name) != 0)) {
            continue;
        }

        memset(&eth, 0, sizeof(eth));

        // name
        strncpy_v(eth.name, ifr->ifr_name, sizeof(eth.name));
        strncpy_v(ifreq.ifr_name, ifr->ifr_name, IFNAMSIZ);

        // addr
        ret = ioctl(sock, SIOCGIFADDR, &ifreq);
        if (ret == 0) {
            eth.addr = ((struct sockaddr_in *) &ifreq.ifr_addr)->sin_addr.s_addr;
            // DEBUGS("addr: %s", inet_ntoa(*(struct in_addr *) &eth.addr));
        } else {
        	// DEBUGS("ret: %d, errno: %d", ret, errno);
        }

        // broadaddr
        ret = ioctl(sock, SIOCGIFBRDADDR, &ifreq);
        if (ret == 0) {
            eth.broadaddr = ((struct sockaddr_in *) &ifreq.ifr_broadaddr)->sin_addr.s_addr;
            // DEBUGS("broadaddr: %s", inet_htoa(*(struct in_addr *) &eth.broadaddr));
        }

        // netmask 
        ret = ioctl(sock, SIOCGIFNETMASK, &ifreq);
        if (ret == 0) {
#if defined(__APPLE__)
            eth.netmask = ((struct sockaddr_in *) &ifreq.ifr_addr)->sin_addr.s_addr;
#else
            eth.netmask = ((struct sockaddr_in *) &ifreq.ifr_netmask)->sin_addr.s_addr;
#endif
            // DEBUGS("netmask: %s", inet_htoa(*(struct in_addr *) &eth.netmask));
        }

        eths[cnt++] = eth;
		if (cnt >= num) {
			break;
		}
	}

	close(sock);

	return cnt;
}

int CreateSock(in_addr_t locaddr, int locport, in_addr_t svraddr, int svrport, int type, int timeo) {
	struct sockaddr_in sa;
	int sock = -1, reuse = 1, ret = 0, nonb = 0;

#define __AT_EXIT__(ret) \
({ \
	typeof(ret) _ret = ret; \
	close(sock); \
	_ret; \
})

	if ((sock = socket(AF_INET, type, 0)) < 0) {
		return __AT_EXIT__(-2);
	}

    ret = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
    if (ret < 0) {
		return __AT_EXIT__(-6);
    }

    if (timeo >= 0) { // set to nonblock for a short time
        ret = SetSockBlock(sock, 0);
        if (ret < 0) {
		    return __AT_EXIT__(-6);
        }
        nonb = 1;
    }

    if ((locaddr != INADDR_ANY) || (locport != 0)) {
        sa.sin_family = AF_INET;
        sa.sin_addr.s_addr = /*htonl(*/locaddr/*)*/;
        sa.sin_port = htons(locport);
        ret = bind(sock, (struct sockaddr *) &sa, sizeof(sa));
        if (ret < 0) {
    		DEBUGS("bind, ret: %d, errno: %d, addr: %s, port: %d", ret, errno,
    				inet_ntoa(*(struct in_addr * ) &locaddr), locport);
            return __AT_EXIT__(-3);
        }
    }

	if ((svraddr != INADDR_ANY) && (svrport != 0)) {
		sa.sin_family = AF_INET;
		sa.sin_addr.s_addr = /*htonl(*/svraddr/*)*/;
		sa.sin_port = htons(svrport);
		ret = connect(sock, (struct sockaddr *) &sa, sizeof(sa));
		if (ret < 0) {
            if (nonb) {
                if (errno != EINPROGRESS) {
			        return __AT_EXIT__(-4);
                }
                ret = CheckSockConnect(sock, timeo); 
                if (ret < 0) { // timeout
			        return __AT_EXIT__(-5);
                }
            } else {
                DEBUGS("connect fail, ret: %d, errno: %d, error: %s", ret, errno, strerror(errno));
			    return __AT_EXIT__(-4);
            }
		}
	}

    if (nonb) { // restore to block
        ret = SetSockBlock(sock, 1);
        if (ret < 0) {
            return __AT_EXIT__(-6);
        }
    }

#undef  __AT_EXIT__

	return sock;
}

int CheckSockConnect(int sock, long msec) {
    struct timeval begin, end;
    long tm = 0, left = 0;
    int ret, serr = 0, ts = 0;
    socklen_t serrlen = sizeof(serr);

    gettimeofday(&begin, NULL);
    for (ts = 0; ts < 70 * 1000; ts++) { // block sock just wait 70s
        if (ts > 0) { // allow wait once at least
            gettimeofday(&end, NULL);
            tm = TimeDiff(&end, &begin);
            if (tm > msec) { // timeout
                break;
            }
        }
        left = msec - tm;
        ret = WaitForFd(sock, /*FD_RD_EV | */FD_EV_WR/* | FD_ERR_EV*/, left);
        if (ret <= 0) {
            continue;
        }
        ret = getsockopt(sock, SOL_SOCKET, SO_ERROR, &serr, &serrlen);
        // DEBUGS("getsockopt: %d, err: %d", ret, serr);
        if (ret < 0) {
            return -1;
        }
        if (serr != 0) {
            continue;
        }
        return 0;
    }

    return -1;
}

int __SetSockTimeout(int sock, int type, int msec) {
	struct timeval tv;

    if (sock < 0) {
        return -1;
    }

	if ((type != SO_SNDTIMEO) && (type != SO_RCVTIMEO)) {
        return -2;
    }

    if (msec < 0) {
        return -3;
    }

    tv.tv_sec = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;

    return setsockopt(sock, SOL_SOCKET, type, &tv, sizeof(tv));
}

ALIAS_FUNC_3(SetSockTimeout, __SetSockTimeout, int, int, int, int);

WEAK_FUNC_2(SetSockBlock, _, int, int sock, int block) {
	int flags;

	if (sock < 0) {
        return -1;
    }

    flags = fcntl(sock, F_GETFL, 0);
    if (flags < 0) {
        return -2;
    }

    if (block == 0) {
        flags = flags | O_NONBLOCK | O_NDELAY;
    } else {
        flags = flags & ~(O_NONBLOCK | O_NDELAY);
    }

    flags = fcntl(sock, F_SETFL, flags);
    if (flags < 0) {
        return -2;
    }

	return 0;
}

ALIAS_FUNC_2(SetSockBlock, _SetSockBlock, int, int, int);

WEAK_FUNC_1(SetNonBlock, _, int, int sock) {
    return SetSockBlock(sock, 0);
}

ALIAS_FUNC_1(SetNonBlock, _SetNonBlock, int, int);

int GetSockType(int sock) {
	int ret, socktype = -1;
	socklen_t optlen = 0;

	optlen = sizeof(socktype);
	ret = getsockopt(sock, SOL_SOCKET, SO_TYPE, &socktype, &optlen);
	if (ret < 0) {
		return -1;
	}

	return socktype;
}


/****************************TIME******************************/
long TimeGet(int flag) {
	struct timeval tv;
	long tm = -1;

	gettimeofday(&tv, NULL);

	switch (flag) {
	case TIME_SEC:
		tm = tv.tv_sec;
		break;
	case TIME_MS:
		tm = (tv.tv_sec % 1000) * 1000 + tv.tv_usec / 1000;
		break;
	case TIME_US:
		tm = tv.tv_usec;
		break;
	}

	return tm;
}

long TimeDiff(struct timeval *tv, struct timeval *base) {
	return (tv->tv_sec - base->tv_sec) * 1000 + (tv->tv_usec - base->tv_usec) / 1000;
}

#ifdef __cplusplus
}
#endif
